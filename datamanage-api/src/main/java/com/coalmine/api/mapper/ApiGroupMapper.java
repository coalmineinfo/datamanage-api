package com.coalmine.api.mapper;

import com.coalmine.api.domain.ApiGroup;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * api组信息表 Mapper 接口
 * </p>
 *
 * @author 尚郑
 * @since 2022-04-01
 */
@Mapper
public interface ApiGroupMapper extends BaseMapper<ApiGroup> {


    @Select("SELECT count(a.id) total ,a.group_id groupId ,b.\"name\" name\n" +
            "FROM api_config a LEFT JOIN api_group b ON a.group_id = b.id\n" +
            "WHERE a.status = 1 AND a.previlege != 2" +
            "GROUP BY a.group_id ,b.\"name\",b.update_time\n" +
            "order by total desc")
    List<Map> getAllGroup();
}
