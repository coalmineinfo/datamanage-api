package com.coalmine.api.mapper;

import com.coalmine.api.domain.DataTypeDict;

import java.util.List;

public interface ApiDataTypeDictMapper {

    List<DataTypeDict> list();
}
