package com.coalmine.api.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.coalmine.api.domain.Entruck;


/**
* @author lhj
* @description 针对表【entruck(装车站管理)】的数据库操作Mapper
* @createDate 2022-10-11 18:12:39
* @Entity com.coalmine.api.domain.Entruck
*/
public interface EntruckMapper extends BaseMapper<Entruck> {


}
