package com.coalmine.api.mapper;

import com.coalmine.api.domain.Contract;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author lhj
* @description 针对表【contract(合同管理)】的数据库操作Mapper
* @createDate 2022-10-12 09:23:35
* @Entity com.coalmine.api.domain.Contract
*/
public interface ContractMapper extends BaseMapper<Contract> {

}




