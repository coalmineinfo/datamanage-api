package com.coalmine.api.mapper;

import com.coalmine.api.domain.ApiAlarm;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;

/**
 * <p>
 * api告警提示表 Mapper 接口
 * </p>
 *
 * @author 尚郑
 * @since 2022-04-01
 */
@Mapper
public interface ApiAlarmMapper extends BaseMapper<ApiAlarm> {
    @Select("select mail from api_alarm where api_id = #{apiId} ")
    String selectMailByApiId(String apiId);

    @Delete("delete from api_alarm where api_id = #{apiId} ")
    int deleteByApiID(String apiId);
}
