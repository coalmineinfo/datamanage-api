package com.coalmine.api.mapper;

import com.coalmine.api.common.ApiDto;
import com.coalmine.api.domain.ApiConfig;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.coalmine.api.domain.ApiGroup;
import org.apache.ibatis.annotations.*;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 尚郑
 * @since 2022-04-01
 */
@Mapper
public interface ApiConfigMapper extends BaseMapper<ApiConfig> {

    @Select("select * from api_config where path=#{path} and status = 1")
    ApiConfig selectByPathOnline(String path);

    Integer selectCountByPath(String path);

    //根据数据源id查询配置信息表数据量，用于判断数据源是否被使用
    Integer countByDatasoure(String id);

    List<ApiConfig> selectByKeyword(@Param("keyword") String keyword,
                                    @Param("field") String field,
                                    @Param("groupId") String groupId);

    @Select("select count(1) from api_config where path=#{path} and id != #{id}")
    Integer selectCountByPathWhenUpdate(@Param("path") String path, @Param("id") String id);

    //根据api分组id查询api分组是否为空
    @Select("select count(1) from api_config where group_id = #{id}")
    int selectCountByGroup(String id);
    //根据id查询api分组
    @Select("SELECT * from api_group where id = #{id}")
    ApiGroup findGroupById(String id);


    @Results(id = "accResultMap", value = {
            @Result(property = "id", column = "id"),
            @Result(property = "name", column = "name"),
            @Result(property = "group_name", column = "groupName")
    })
    @Select("select t1.id,t1.name,t2.name as group_name from api_config t1 join api_group t2 on t1.group_id = t2.id")
    List<ApiDto> getAllDetail();

    /**
     * 查询最新发布的资源默认5个
     * @return
     */
    List<Map> getNewestList();

    /**
     * 查询最热已发布的资源默认5个
     * @return
     */
    List<Map> getHotList();

    List<Map> searchResource(@Param("name") String name,@Param("apiId") String apiId,@Param("groupId") String groupId);

    @Select("select id,name  from api_config \n" +
            "WHERE group_id = #{groupId}\n" +
            "and status = 1 and previlege != 2\n" +
            "order by update_time desc")
    List<Map> getByGroupId(String groupId);

    @Select("select count(*) from api_config")
    String searchResourceCount();

    List<ApiConfig> listByGroupIds(@Param("groupIds") List<String> groupIds);
}
