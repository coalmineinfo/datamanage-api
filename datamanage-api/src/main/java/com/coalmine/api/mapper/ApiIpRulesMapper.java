package com.coalmine.api.mapper;

import com.coalmine.api.domain.ApiIpRules;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * 系统黑白名单信息表 Mapper 接口
 * </p>
 *
 * @author 尚郑
 * @since 2022-04-01
 */
public interface ApiIpRulesMapper extends BaseMapper<ApiIpRules> {

    @Update("update api_firewall set status = 'off',update_by = #{username} ,update_time = now()")
    int turnOff(@Param("username")String username);

    @Update("update api_firewall set status = 'on' , mode = #{mode} ,update_by = #{username} ,update_time = now()")
    int turnOn(@Param("mode") String mode,@Param("username") String username);

    @Update("insert into api_ip_rules (id,type,ip,create_by,create_time) VALUES (#{id},#{type},#{ip},#{username},now())")
    int saveIP(@Param("id") String id,@Param("ip") String ip, @Param("type") String type,@Param("username") String username);

    @Select("select status, mode from api_firewall")
    Map<String, String> getStatus();

    @Select("select id, type, ip from api_ip_rules")
    List<Map<String, String>> getIPRule();

    @Select("select ip from api_ip_rules where type = 'white'")
    String getWhiteIP();

    @Select("select ip from api_ip_rules where type = 'black'")
    String getBlackIP();

}
