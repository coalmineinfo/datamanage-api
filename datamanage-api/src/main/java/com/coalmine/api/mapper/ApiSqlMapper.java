package com.coalmine.api.mapper;

import com.coalmine.api.domain.ApiSql;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 * api的sql信息表 Mapper 接口
 * </p>
 *
 * @author 尚郑
 * @since 2022-04-01
 */
@Mapper
public interface ApiSqlMapper extends BaseMapper<ApiSql> {
    @Select("select * from api_sql where api_id = #{id}")
    List<ApiSql> selectByApiId(String id);

    @Select("select * from api_sql ")
    List<ApiSql> selectAllApiSql();

    @Delete("delete from api_sql where api_id = #{id}")
    void deleteByApiID(String id);

    List<ApiSql> selectByApiIds(@Param("ids") List<String> ids);



}
