package com.coalmine.api.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;

import  com.coalmine.api.domain.AddTransport;

/**
* @author lhj
* @description 针对表【transport(拉运信息)】的数据库操作Mapper
* @createDate 2022-10-11 09:59:27
* @Entity generator.domain.Transport
*/
public interface TransportMapper extends BaseMapper<AddTransport> {


}
