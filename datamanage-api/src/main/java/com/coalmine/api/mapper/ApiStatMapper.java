package com.coalmine.api.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.coalmine.api.domain.ApiStat;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface ApiStatMapper  extends BaseMapper<ApiStat> {

    List<ApiStat> statByUser();

    List<ApiStat> statByDay(@Param("day") String day);

    List<ApiStat> statByDayRange(
                            @Param("startDay") String startDay,
                            @Param("endDay") String endDay);

    List<ApiStat> statByMonth();
}
