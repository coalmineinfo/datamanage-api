package com.coalmine.api.mapper;

import com.coalmine.api.domain.Carriage;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author lhj
* @description 针对表【carriage(火车装车订单表)】的数据库操作Mapper
* @createDate 2022-10-12 10:36:10
* @Entity com.coalmine.api.domain.Carriage
*/
public interface CarriageMapper extends BaseMapper<Carriage> {

}




