package com.coalmine.api.mapper;

import com.coalmine.api.domain.ApiDatasource;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 尚郑
 * @since 2022-04-01
 */
public interface ApiDatasourceMapper extends BaseMapper<ApiDatasource> {

}
