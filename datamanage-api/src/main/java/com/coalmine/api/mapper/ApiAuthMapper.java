package com.coalmine.api.mapper;

import com.coalmine.api.domain.ApiAuth;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * <p>
 * api权限表 Mapper 接口
 * </p>
 *
 * @author 尚郑
 * @since 2022-04-01
 */
@Mapper
public interface ApiAuthMapper extends BaseMapper<ApiAuth> {

    @Select("select group_id from api_auth where token_id = #{tokenId} ")
    List<String> selectByTokenId(String tokenId);

    @Update("delete from api_auth where token_id = #{tokenId}")
    int deleteByTokenId(String tokenId);
}
