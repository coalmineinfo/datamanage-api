package com.coalmine.api.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.coalmine.api.domain.ApiConfig;
import com.coalmine.api.domain.ApiToken;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * <p>
 * api的token信息表 Mapper 接口
 * </p>
 *
 * @author 尚郑
 * @since 2022-04-01
 */
@Mapper
public interface ApiTokenMapper extends BaseMapper<ApiToken> {

    @Select("select * from api_token where token = #{token}")
    ApiToken selectByToken(String token);

    @Select("SELECT a.id FROM api_config a " +
            "LEFT JOIN api_approve b on a.id = b.api_id " +
            " WHERE b.token_id = #{id}" +
            " AND path = #{api}" +
            " AND approve_type != 2 ")
    String checkToken(@Param("api") String api, @Param("id") String id);

    @Select("select * from api_token order by update_time desc")
    List<ApiToken> getAll();

    int changePassword(@Param("name") String name,
                       @Param("oldPassword") String oldPassword,
                       @Param("newPassword") String newPassword);

    @Select("SELECT * FROM api_config WHERE  path = #{api}")
    ApiConfig getTokenByPath(String api);
}
