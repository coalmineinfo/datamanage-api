package com.coalmine.api.mapper;

import com.coalmine.api.domain.CarriageInfo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
* @author lhj
* @description 针对表【carriageinfo(火车装车任务详情)】的数据库操作Mapper
* @createDate 2022-10-12 10:36:19
* @Entity com.coalmine.api.domain.Carriageinfo
*/
public interface CarriageinfoMapper extends BaseMapper<CarriageInfo> {

}




