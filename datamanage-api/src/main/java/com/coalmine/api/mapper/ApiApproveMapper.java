package com.coalmine.api.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.coalmine.api.domain.ApiApprove;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;


/**
 * <p>
 * api的审批信息表 Mapper 接口
 * </p>
 *
 * @author 尚郑
 * @since 2022-06-02
 */
@Mapper
public interface ApiApproveMapper extends BaseMapper<ApiApprove> {

    @Select("SELECT A.* ,B.\"name\" apiName ,C.\"name\" tokenName FROM \"api_approve\" A LEFT JOIN api_group B ON A.group_id=B.\"id\" LEFT JOIN api_token C ON A.token_id=C.\"id\"")
    List<ApiApprove> getAll();

    @Select("SELECT A.* ,B.\"name\" apiName ,C.\"name\" tokenName FROM \"api_approve\" A LEFT JOIN api_group B ON A.group_id=B.\"id\" LEFT JOIN api_token C ON A.token_id=C.\"id\" WHERE A.\"id\"=#{approveId}")
    ApiApprove findApproveById(String approveId);

    //@Select("SELECT DISTINCT C.\"id\" tokenId, C.\"name\" tokenName FROM \"api_approve\" A LEFT JOIN api_group B ON A.group_id=B.\"id\" LEFT JOIN api_token C ON A.token_id=C.\"id\"")

   @Select("SELECT DISTINCT C.\"id\" tokenId,COALESCE(C.note,'null') applyUserName, C.\"name\" tokenName FROM \"api_approve\" A LEFT JOIN api_group B ON A.group_id=B.\"id\" LEFT JOIN api_token C ON A.token_id=C.\"id\"\n")
    List<ApiApprove> getAllApplyUser();

    List<ApiApprove> select(@Param("applyUserId") String applyUserId,@Param("groupId") String groupId,@Param("approveType") Integer approveType);

    List<ApiApprove> listByApiUser(@Param("apiId") String apiId, @Param("token") String token);

    int apply(ApiApprove apiApprove);

    @Select("SELECT id FROM api_approve WHERE token_id = #{tokenId} AND api_id = #{apiId} AND  approve_type = '1'")
    String checkApprove(@Param("apiId") String apiId,@Param("tokenId") String tokenId);
}
