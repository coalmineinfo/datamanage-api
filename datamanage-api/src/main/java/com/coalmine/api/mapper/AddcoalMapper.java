package com.coalmine.api.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.coalmine.api.domain.Addcoal;


/**
* @author lhj
* @description 针对表【addcoal(加卸煤管理)】的数据库操作Mapper
* @createDate 2022-10-11 16:10:30
* @Entity com.coalmine.api.domain.Addcoal
*/
public interface AddcoalMapper extends BaseMapper<Addcoal> {


}
