package com.coalmine.api.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.coalmine.api.common.ApiLogDto;
import com.coalmine.api.domain.ApiLog;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * api的sql信息表 Mapper 接口
 * </p>
 *
 * @author 尚郑
 * @since 2022-04-01
 */
@Mapper
public interface ApiLogMapper extends BaseMapper<ApiLog> {

    List<ApiLogDto> selectApiLogList(ApiLogDto apiLogDto);

    ApiLogDto getApiLog(String id);

    /**
     * 批量插入数据列表
     *
     * @param dataList 数据列表
     */
    void insertApiLogList(@Param("list") List<ApiLog> dataList);
}
