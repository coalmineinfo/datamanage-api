package com.coalmine.api.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.coalmine.api.domain.ApiDatasource;
import com.coalmine.api.domain.ApiDatasourceConf;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author zjx
 * @since 2022-04-08
 */
public interface ApiDatasourceConfMapper extends BaseMapper<ApiDatasourceConf> {

}
