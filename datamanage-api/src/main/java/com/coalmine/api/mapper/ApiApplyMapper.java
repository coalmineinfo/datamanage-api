package com.coalmine.api.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.coalmine.api.domain.ApiAlarm;
import com.coalmine.api.domain.ApiApprove;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * <p>
 * api告警提示表 Mapper 接口
 * </p>
 *
 * @author 尚郑
 * @since 2022-04-01
 */
@Mapper
public interface ApiApplyMapper extends BaseMapper<ApiApprove> {

    List<ApiApprove> listByToken(@Param("tokenId") String tokenId, @Param("type") String type);

    List<ApiApprove> listByTokenApi(@Param("tokenId") String tokenId, @Param("apiId") String apiId);
}
