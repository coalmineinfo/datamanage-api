package com.coalmine.api.mapper;

import com.coalmine.api.domain.ApiFirewall;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 尚郑
 * @since 2022-04-01
 */
public interface ApiFirewallMapper extends BaseMapper<ApiFirewall> {

}
