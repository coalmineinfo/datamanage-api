package com.coalmine.api.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * API统计类型
 *
 * @author wangleibo
 * @since 2022-09-13
 */
@Getter
@AllArgsConstructor
public enum EApiStatType {

    DAY(0, "按天统计"),
    MONTH(1, "按月统计");

    private final int code;
    private final String remark;
}
