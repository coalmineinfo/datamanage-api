package com.coalmine.api.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * API状态
 *
 * @author wangleibo
 * @since 2022-09-07
 */
@Getter
@AllArgsConstructor
public enum EApiStatus {

    OFFLINE(0, "下线"),
    ONLINE(1, "上线");

    private final int code;
    private final String remark;
}
