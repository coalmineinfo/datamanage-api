package com.coalmine.api.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * 审批状态
 *
 * @author wangleibo
 * @since 2022-09-07
 */
@Getter
@AllArgsConstructor
public enum EApproveStatus {

    PENDING(0, "待处理"),
    APPROVED(1, "已通过"),
    REJECTED(2, "未通过");

    private final int code;
    private final String remark;
}
