package com.coalmine.api.enums;

import lombok.AllArgsConstructor;
import lombok.Getter;

/**
 * API权限
 *
 * @author wangleibo
 * @since 2022-09-07
 */
@Getter
@AllArgsConstructor
public enum EApiPermission {

    PRIVATE(0, "私有"),
    PUBLIC(1, "公开"),
    INNER(2, "内部");

    private final int code;
    private final String remark;
}
