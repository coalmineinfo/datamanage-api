package com.coalmine.api.service.impl;

import com.coalmine.api.domain.ApiAlarm;
import com.coalmine.api.mapper.ApiAlarmMapper;
import com.coalmine.api.service.IApiAlarmService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * api告警提示表 服务实现类
 * </p>
 *
 * @author 尚郑
 * @since 2022-04-01
 */
@Service
public class ApiAlarmServiceImpl extends ServiceImpl<ApiAlarmMapper, ApiAlarm> implements IApiAlarmService {

}
