package com.coalmine.api.service;

import com.coalmine.api.domain.ApiDatasource;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 尚郑
 * @since 2022-04-01
 */
public interface IApiDatasourceService extends IService<ApiDatasource> {

    /**
     * 新增数据源
     */
    int add(ApiDatasource apiDatasource);

    /**
     * 获取数据源列表
     */
    List<ApiDatasource> getAll();

    /**
     * 根据ids批量查询
     */
    List<ApiDatasource> selectBatch(List<String> collect);

    /**
     * 根据ids批量插入
     */
    void insertBatch(List<ApiDatasource> list);

    /**
     * 根据主键更新
     */
    int update(ApiDatasource apiDatasource);

    /**
     * 根据id获取详情
     */
    ApiDatasource detail(String id);

    /**
     * 根据id删除
     */
    int delete(String id);

    /**
     * 加载数据源缓存数据
     */
    void loadingDatasourceCache();

    /**
     * 清空数据源缓存数据
     */
    void clearDatasourceCache();
}
