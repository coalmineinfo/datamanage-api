package com.coalmine.api.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.coalmine.api.domain.ApiDatasourceConf;

import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 尚郑
 * @since 2022-04-01
 */
public interface IApiDatasourceConfService extends IService<ApiDatasourceConf> {

    /**
     * 获取所有数据源默认配置列表
     */
    List<ApiDatasourceConf> getAll();

    /**
     * 根据id获取数据源默认配置详情
     */
    ApiDatasourceConf detail(String id);

    /**
     * 加载数据源默认配置缓存
     */
    void loadingDsconfCache();

    /**
     * 加载数据源默认配置缓存
     */
    void clearDsconfCache();
}
