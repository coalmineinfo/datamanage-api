package com.coalmine.api.service;


import com.coalmine.api.domain.ApiConfig;
import com.coalmine.api.domain.ApiToken;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;

/**
 * <p>
 * api的审批信息表 服务类
 * </p>
 *
 * @author 尚郑
 * @since 2022-06-02
 */
public interface IApiResourceService  {


    Map<String, Object> getDetail(ApiConfig apiConfig, HttpServletRequest request);

    Map<String, Object> getIndex(List<Map> apiGroupList);

    ApiToken getToken(String name, String password);

    List<Map> getAllGroup();


    ApiConfig getById(String id);

    Map<String, Object> getInfo(List<Map> apiGroupList);

    List<Map> searchResource(String name, String apiId, String groupId);

    int apply(String applyReason, String groupId, String tokenId, String apiId);

    String searchResourceCount();

}
