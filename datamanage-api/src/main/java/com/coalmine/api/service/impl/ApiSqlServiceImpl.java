package com.coalmine.api.service.impl;

import com.coalmine.api.domain.ApiSql;
import com.coalmine.api.mapper.ApiSqlMapper;
import com.coalmine.api.service.IApiSqlService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * api的sql信息表 服务实现类
 * </p>
 *
 * @author 尚郑
 * @since 2022-04-01
 */
@Service
public class ApiSqlServiceImpl extends ServiceImpl<ApiSqlMapper, ApiSql> implements IApiSqlService {

}
