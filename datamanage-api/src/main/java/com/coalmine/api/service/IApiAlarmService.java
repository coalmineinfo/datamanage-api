package com.coalmine.api.service;

import com.coalmine.api.domain.ApiAlarm;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * api告警提示表 服务类
 * </p>
 *
 * @author 尚郑
 * @since 2022-04-01
 */
public interface IApiAlarmService extends IService<ApiAlarm> {

}
