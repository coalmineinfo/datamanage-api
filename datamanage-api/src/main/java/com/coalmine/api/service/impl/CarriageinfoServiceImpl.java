package com.coalmine.api.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.coalmine.api.domain.CarriageInfo;
import com.coalmine.api.service.CarriageinfoService;
import com.coalmine.api.mapper.CarriageinfoMapper;
import org.springframework.stereotype.Service;

/**
* @author lhj
* @description 针对表【carriageinfo(火车装车任务详情)】的数据库操作Service实现
* @createDate 2022-10-12 10:36:19
*/
@Service
public class CarriageinfoServiceImpl extends ServiceImpl<CarriageinfoMapper, CarriageInfo>
    implements CarriageinfoService{

}




