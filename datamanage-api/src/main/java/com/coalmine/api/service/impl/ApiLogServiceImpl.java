package com.coalmine.api.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.coalmine.api.common.ApiLogDto;
import com.coalmine.api.domain.ApiLog;
import com.coalmine.api.mapper.ApiLogMapper;
import com.coalmine.api.service.IApiLogService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ApiLogServiceImpl extends ServiceImpl<ApiLogMapper, ApiLog> implements IApiLogService {
    @Autowired
    ApiLogMapper apiLogMapper;

    @Override
    public List<ApiLogDto> selectApiLogList(ApiLogDto apiLogDto) {
        return apiLogMapper.selectApiLogList(apiLogDto);
    }

    @Override
    public ApiLogDto getApiLog(String id) {
        return apiLogMapper.getApiLog(id);
    }

    @Override
    public void insertApiLogList(List<ApiLog> dataList) {
        apiLogMapper.insertApiLogList(dataList);
    }
}
