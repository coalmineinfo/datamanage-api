package com.coalmine.api.service;

import com.alibaba.fastjson.JSONObject;
import com.coalmine.api.domain.Carriage;
import com.baomidou.mybatisplus.extension.service.IService;
import com.coalmine.api.domain.CarriageInfo;

import java.util.List;

/**
* @author lhj
* @description 针对表【carriage(火车装车订单表)】的数据库操作Service
* @createDate 2022-10-12 10:36:10
*/
public interface CarriageService extends IService<Carriage> {



    boolean addCarriage(Carriage carriage);
}
