package com.coalmine.api.service;

import com.coalmine.api.domain.CarriageInfo;
import com.baomidou.mybatisplus.extension.service.IService;

/**
* @author lhj
* @description 针对表【carriageinfo(火车装车任务详情)】的数据库操作Service
* @createDate 2022-10-12 10:36:19
*/
public interface CarriageinfoService extends IService<CarriageInfo> {

}
