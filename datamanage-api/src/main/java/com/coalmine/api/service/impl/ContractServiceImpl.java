package com.coalmine.api.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.coalmine.api.domain.Contract;
import com.coalmine.api.service.ContractService;
import com.coalmine.api.mapper.ContractMapper;
import com.coalmine.api.util.GetConnectionUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

/**
 * @author lhj
 * @description 针对表【contract(合同管理)】的数据库操作Service实现
 * @createDate 2022-10-12 09:23:35
 */
@Service
public class ContractServiceImpl extends ServiceImpl<ContractMapper, Contract>
        implements ContractService {
    @Value("${yxxt.datasource.url}")
    private String url;
    @Value("${yxxt.datasource.username}")
    private String username;
    @Value("${yxxt.datasource.password}")
    private String password;

    @Override
    public boolean addContract(List<Contract> addContractList) {
        Connection conn = null;
        try {
            conn = GetConnectionUtil.GetConnection(url, username, password);
            String sql = "INSERT INTO contract (BCode,Customer,CoalType,Quota,SigTime,STime,ETime,Price,AreaType,BoardingTicket,Margin,Status)\n" +
                    "VALUES (?,?,?,?,?,?,?,?,?,?,?,?)";
            PreparedStatement ps = conn.prepareStatement(sql);
            for(Contract addContract : addContractList){
                ps.setString(1,addContract.getBCode());
                ps.setString(2,addContract.getCustomer());
                ps.setString(3,addContract.getCoalType());
                ps.setString(4,addContract.getQuota());
                ps.setString(5,addContract.getSigTime());
                ps.setString(6,addContract.getSTime());
                ps.setString(7,addContract.getETime());
                ps.setDouble(8,addContract.getPrice());
                ps.setString(9,addContract.getAreaType());
                ps.setString(10,addContract.getBoardingTicket());
                ps.setString(11,addContract.getMargin());
                ps.setString(12,addContract.getStatus());
                ps.addBatch();
            }
              ps.executeBatch();
            return true;
        } catch (SQLException throwables) {
            if (conn!=null){
                GetConnectionUtil.releaseConnection(conn);
            }
            throwables.printStackTrace();
            return false;
        }
    }
}




