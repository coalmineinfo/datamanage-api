package com.coalmine.api.service;

import com.coalmine.api.domain.Contract;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
* @author lhj
* @description 针对表【contract(合同管理)】的数据库操作Service
* @createDate 2022-10-12 09:23:35
*/
public interface ContractService extends IService<Contract> {

    boolean addContract(List<Contract> addContractList);
}
