package com.coalmine.api.service.impl;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.coalmine.api.domain.AddTransport;
import com.coalmine.api.mapper.TransportMapper;

import com.coalmine.api.util.GetConnectionUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import com.coalmine.api.service.TransportService;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.util.List;

/**
* @author lhj
* @description 针对表【transport(拉运信息)】的数据库操作Service实现
* @createDate 2022-10-10 15:26:11
*/
@Slf4j
@Service
public class TransportServiceImpl extends ServiceImpl<TransportMapper, AddTransport>
    implements TransportService {

    @Autowired
    private TransportService transportService;

    @Value("${yxxt.datasource.url}")
    private String url;
    @Value("${yxxt.datasource.username}")
    private String username;
    @Value("${yxxt.datasource.password}")
    private String password;

    @Override
    public boolean addTransport(List<AddTransport> transportList) {
        Connection conn = null;

        try {
            conn = GetConnectionUtil.GetConnection(url, username, password);
            String sql="INSERT INTO transport (PCCode,BCode,CarNo,CarType,BasisWeight,DriverName,Phone,Customer,Adr,CoalType,KTime,ITime,ZTime,OTime,PWeight,MWeight,JWeight)\n" +
                    "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
            PreparedStatement ps = conn.prepareStatement(sql);

            // boolean b = transportService.saveBatch(transportList);
            for (AddTransport transport: transportList) {
                ps.setInt(1,transport.getPccode());
                ps.setString(2,transport.getBcode());
                ps.setString(3,transport.getCarno());
                ps.setString(4,transport.getCartype());
                ps.setDouble(5,transport.getBasisweight());
                ps.setString(6,transport.getDrivername());
                ps.setString(7,transport.getPhone());
                ps.setString(8,transport.getCustomer());
                ps.setString(9,transport.getAdr());
                ps.setString(10,transport.getCoaltype());
                ps.setString(11,transport.getKtime());
                ps.setString(12,transport.getItime());
                ps.setString(13,transport.getZtime());
                ps.setObject(14,transport.getOtime());
                ps.setDouble(15,transport.getPweight());
                ps.setDouble(16,transport.getMweight());
                ps.setDouble(17,transport.getJweight());
                ps.addBatch();
                ps.executeBatch();
            }
            return true;
        } catch (Exception throwables) {
            if (conn != null) {
                GetConnectionUtil.releaseConnection(conn);
            }
            throwables.printStackTrace();
            return false;
        }
    }
}





