package com.coalmine.api.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.coalmine.api.domain.Addcoal;
import com.coalmine.api.mapper.AddcoalMapper;
import com.coalmine.api.service.AddcoalService;
import com.coalmine.api.util.GetConnectionUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

/**
* @author lhj
* @description 针对表【addcoal(加卸煤管理)】的数据库操作Service实现
* @createDate 2022-10-11 16:10:30
*/
@Service
public class AddcoalServiceImpl extends ServiceImpl<AddcoalMapper, Addcoal>
implements AddcoalService{


    @Value("${yxxt.datasource.url}")
    private String url;
    @Value("${yxxt.datasource.username}")
    private String username;
    @Value("${yxxt.datasource.password}")
    private String password;


    @Override
    public boolean addCoal(List<Addcoal> addCoalList) {
        Connection conn=null;
        try {
            conn = GetConnectionUtil.GetConnection(url, username, password);
            String sql ="INSERT INTO addcoal (ID,CarNo,CoalType,JXWeight,CompleteTime,CoalStation,RecordPeople,RecordTime)\n" +
                    "VALUES (?,?,?,?,?,?,?,?)";
            PreparedStatement ps = conn.prepareStatement(sql);
            for (Addcoal addcoal: addCoalList) {

                ps.setString(1,addcoal.getID());
                ps.setString(2,addcoal.getCarNo());
                ps.setString(3,addcoal.getCoalType());
                ps.setDouble(4,addcoal.getJXWeight());
                ps.setString(5,addcoal.getCompleteTime());
                ps.setString(6,addcoal.getCoalStation());
                ps.setString(7,addcoal.getRecordPeople());
                ps.setString(8,addcoal.getRecordTime());
                ps.addBatch();
                ps.executeBatch();
            }
            return true;

        } catch (SQLException throwables) {
            if (conn!=null){
                GetConnectionUtil.releaseConnection(conn);
            }
            throwables.printStackTrace();
            return false;
        }

    }
}
