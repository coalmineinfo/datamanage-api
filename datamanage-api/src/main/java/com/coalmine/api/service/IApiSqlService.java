package com.coalmine.api.service;

import com.coalmine.api.domain.ApiSql;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * api的sql信息表 服务类
 * </p>
 *
 * @author 尚郑
 * @since 2022-04-01
 */
public interface IApiSqlService extends IService<ApiSql> {

}
