package com.coalmine.api.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.coalmine.api.domain.ApiApprove;
import com.coalmine.api.domain.ApiToken;
import com.coalmine.api.enums.EApproveStatus;
import com.coalmine.api.mapper.ApiApproveMapper;
import com.coalmine.api.mapper.ApiAuthMapper;
import com.coalmine.api.mapper.ApiTokenMapper;
import com.coalmine.api.service.IApiApproveService;
import com.coalmine.api.util.UUIDUtil;
import com.coalmine.common.constant.Constants;
import com.coalmine.common.core.domain.AjaxResult;
import com.coalmine.common.core.redis.RedisCache;
import com.coalmine.common.utils.SecurityUtils;
import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * <p>
 * api的审批信息表 服务实现类
 * </p>
 *
 * @author 尚郑
 * @since 2022-06-02
 */
@Service
public class ApiApproveServiceImpl extends ServiceImpl<ApiApproveMapper, ApiApprove> implements IApiApproveService {

    @Autowired
    private ApiApproveMapper apiApproveMapper;

    @Autowired
    private ApiTokenMapper apiTokenMapper;
    @Autowired
    private ApiTokenServiceImpl apiTokenService;

    @Autowired
    ApiAuthMapper apiAuthMapper;

    @Autowired
    private RedisCache redisCache;


    @Override
    public List<ApiApprove> getAll() {
        return apiApproveMapper.getAll();
    }

    @Override
    public ApiApprove findApproveById(String approveId) {
        return apiApproveMapper.findApproveById(approveId);
    }

    @Transactional
    public AjaxResult updateApproveById(ApiApprove approve) {
        UpdateWrapper<ApiApprove> updateWrapper = new UpdateWrapper<>();
        String token = DigestUtils.md5Hex(UUID.randomUUID().toString());
        String username = SecurityUtils.getLoginUser().getUsername();
        String groupId = approve.getGroupId();
//        ApiToken apiToken = new ApiToken();
//        if (EApproveStatus.APPROVED.getCode() == approve.getApproveType()) {
//            apiToken.setId(UUIDUtil.getUUID());
//        }
//        long time=31536000000l;
//        long newExpire = System.currentTimeMillis() + time;
//        apiToken.setExpire(newExpire);
//        apiToken.setName(approve.getApplyUserName());
//        apiToken.setToken(token);
//        apiToken.setCreateBy(username);
//        apiToken.setCreateTime(new Date());
        //apiToken.setPassword(DigestUtils.md5Hex("123456"));
        updateWrapper.eq("id", approve.getId())
                .set("approve_name", username)
                .set("approve_time", new Date())
                .set("approve_reason", approve.getApproveReason())
                .set("approve_type", approve.getApproveType());
        int i = apiApproveMapper.update(null, updateWrapper);
        if (i > 0) {
//            if (EApproveStatus.APPROVED.getCode() == approve.getApproveType()) {
//                apiTokenService.auth(apiToken.getId(), Arrays.asList(groupId), username);
//                apiTokenMapper.insert(apiToken);
//                redisCache.setCacheObject(Constants.API_TOKEN_KEY + token, apiToken);
//                redisCache.expire(Constants.API_TOKEN_KEY + token, newExpire, TimeUnit.MILLISECONDS);
//            }
            return AjaxResult.success("审批成功");
        } else {
            return AjaxResult.error("审批失败");
        }
    }

    @Override
    public int apply(ApiApprove apiApprove) {
        return apiApproveMapper.apply(apiApprove);
    }

    @Override
    public List<ApiApprove> select(String applyUserId, String groupId, Integer approveType) {
        return apiApproveMapper.select(applyUserId, groupId, approveType);
    }

    @Override
    public List<ApiApprove> getAllApplyUser() {
        return apiApproveMapper.getAllApplyUser();
    }

    @Override
    public List<ApiApprove> selectListByWrapper(ApiApprove apiApprove) {
        //查询条件
        QueryWrapper<ApiApprove> q = new QueryWrapper<>();
        q.eq("api_id", apiApprove.getApiId())
                .eq("group_id", apiApprove.getGroupId())
                .eq("apply_user_id", apiApprove.getApplyUserId())
                .orderByDesc("apply_time");
        List<ApiApprove> list = apiApproveMapper.selectList(q);
        return list;
    }

    @Override
    public List<ApiApprove> listByApiUser(String apiId, String token) {
        return apiApproveMapper.listByApiUser(apiId, token);
    }

    @Override
    public String checkApprove(String apiId, String tokenId) {
        return apiApproveMapper.checkApprove(apiId,tokenId);
    }
}



