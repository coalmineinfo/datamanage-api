package com.coalmine.api.service;

import com.coalmine.api.domain.ApiFirewall;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 尚郑
 * @since 2022-04-01
 */
public interface IApiFirewallService extends IService<ApiFirewall> {

}
