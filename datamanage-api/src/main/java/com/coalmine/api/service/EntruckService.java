package com.coalmine.api.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.coalmine.api.domain.Entruck;


import java.util.List;

/**
* @author lhj
* @description 针对表【entruck(装车站管理)】的数据库操作Service
* @createDate 2022-10-11 18:12:39
*/
public interface EntruckService extends IService<Entruck> {

    boolean addEntruck(List<Entruck> addEntruckList);
}
