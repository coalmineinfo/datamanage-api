package com.coalmine.api.service.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.coalmine.api.domain.ApiGroup;
import com.coalmine.api.mapper.ApiConfigMapper;
import com.coalmine.api.mapper.ApiGroupMapper;
import com.coalmine.api.service.IApiGroupService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.coalmine.api.util.UUIDUtil;
import com.coalmine.common.core.domain.AjaxResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.CacheManager;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import java.sql.Timestamp;
import java.util.Date;
import java.util.List;
import java.util.Map;

import static com.coalmine.common.utils.SecurityUtils.getUsername;

/**
 * <p>
 * api组信息表 服务实现类
 * </p>
 *
 * @author 尚郑
 * @since 2022-04-01
 */
@Service
public class ApiGroupServiceImpl extends ServiceImpl<ApiGroupMapper, ApiGroup> implements IApiGroupService {
    @Autowired
    CacheManager cacheManager;

    @Autowired
    ApiGroupMapper apiGroupMapper;

    @Autowired
    ApiConfigMapper apiConfigMapper;


    @Transactional
    public AjaxResult addApiGroup(ApiGroup apiGroup) {

        apiGroup.setId(UUIDUtil.getUUID());
        apiGroup.setCreateTime(new Date());
        apiGroup.setUpdateTime(new Date());
        //创建api分组
        int insert = apiGroupMapper.insert(apiGroup);
        if (insert > 0) {
            return AjaxResult.success("创建API分组成功！");
        } else {
            return AjaxResult.error("创建API分组失败！");
        }


    }

    @Transactional
    public AjaxResult updateApiGroup(ApiGroup apiGroup) {
        apiGroup.setUpdateTime(new Date());
        //apiGroup.setCreateBy(apiGroup.getCreateBy());
        int updateById = apiGroupMapper.updateById(apiGroup);
        if (updateById > 0) {
            return AjaxResult.success("更新API分组成功！");
        } else {
            return AjaxResult.error("更新API分组失败！");
        }

    }

    @Transactional
    public AjaxResult deleteApiGroupById(String id) {
        int size = apiConfigMapper.selectCountByGroup(id);
        if (size > 0) {
            return AjaxResult.error("api分组被使用, 不能删除");
        } else {
            apiGroupMapper.deleteById(id);

            return AjaxResult.success("删除api分组成功");
        }

    }

    @Override
    public ApiGroup findGroupById(String id) {
        return apiConfigMapper.findGroupById(id);
    }

    @Override
    public List<ApiGroup> getAll() {

        return apiGroupMapper.selectList(null);
    }

    @Override
    public List<ApiGroup> getAllGroupList() {
        QueryWrapper<ApiGroup> queryWrapper = new QueryWrapper();

         List<ApiGroup> list = apiGroupMapper.selectList(queryWrapper.orderByDesc("update_time"));
         return list;
    }
    //导出api分组
    @Override
    public List<ApiGroup> selectBatch(List<String> ids) {
        return apiGroupMapper.selectBatchIds(ids);
    }
    //导入api分组
    @Override
    public void insertBatch(List<ApiGroup> configs) {
        String username = getUsername();
        configs.stream().forEach(t->{
            t.setCreateBy(username);
            t.setCreateTime(new Date());
            t.setUpdateBy(username);
            t.setUpdateTime(new Date());
            apiGroupMapper.insert(t);
        });
    }

    @Override
    public List<Map> getAllGroup() {
        return apiGroupMapper.getAllGroup();
    }


}
