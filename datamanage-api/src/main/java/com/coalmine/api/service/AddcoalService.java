package com.coalmine.api.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.coalmine.api.domain.Addcoal;


import java.util.List;

/**
* @author lhj
* @description 针对表【addcoal(加卸煤管理)】的数据库操作Service
* @createDate 2022-10-11 16:10:30
*/
public interface AddcoalService extends IService<Addcoal> {


    boolean addCoal(List<Addcoal> addCoalList);
}
