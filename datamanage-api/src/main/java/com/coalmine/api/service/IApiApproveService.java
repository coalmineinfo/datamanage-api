package com.coalmine.api.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.coalmine.api.domain.ApiApprove;
import com.coalmine.common.core.domain.AjaxResult;

import java.util.List;

/**
 * <p>
 * api的审批信息表 服务类
 * </p>
 *
 * @author 尚郑
 * @since 2022-06-02
 */
public interface IApiApproveService extends IService<ApiApprove> {

    List<ApiApprove> getAll();

    ApiApprove findApproveById(String approveId);

    AjaxResult updateApproveById(ApiApprove approve);

    int apply(ApiApprove apiApprove);

    List<ApiApprove> select(String applyUserId, String groupId,Integer approveType);

    List<ApiApprove> getAllApplyUser();

    //多条件查询审批表
    List<ApiApprove> selectListByWrapper(ApiApprove apiApprove);

    List<ApiApprove> listByApiUser(String apiId, String token);

    String checkApprove(String apiId, String tokenId);
}
