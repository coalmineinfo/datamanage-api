package com.coalmine.api.service;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.service.IService;
import com.coalmine.api.domain.ApiConfig;
import com.coalmine.api.domain.ApiSql;

import java.util.List;
import java.util.Map;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 尚郑
 * @since 2022-04-01
 */
public interface IApiConfigService extends IService<ApiConfig> {
     //创建api
     void add(ApiConfig apiConfig);

     //更新api
     void update(ApiConfig apiConfig);

     void copy(String id);

     List<ApiConfig> getAll();

     ApiConfig detail(String id);

     List<ApiConfig> search(String keyword, String field, String groupId);

     Integer delete(String id);

     String getPath(String id);

     void online(String id, String path);

     void offline(String id, String path);

     ApiConfig getConfig(String path);

     JSONArray getAllDetail();

     String apiDocs(List<String> ids);

     List<Map<String, Object>> apiDocx(List<String> ids);

     JSONObject selectBatch(List<String> ids);

     void insertBatch(List<ApiConfig> configs, List<ApiSql> sqls, String userName);

     List<JSONObject> getAllColumns(String sourceId, String table);

     /**
      * 获取最新发布的信息资源 默认前五个
      *
      * @return
      */
     List<Map> getNewestList();

     /**
      * 获取最热发布的信息资源 默认前五个
      * @return
      */
     List<Map> getHotList();

     List<Map> searchResource(String name, String apiId, String groupId);

     List<Map> getByGroupId(String groupId);

     List<ApiSql> getSqlByConfigId(String id);

     String searchResourceCount();

     List<ApiConfig> listByGroupIds(List<String> groupIds);
}
