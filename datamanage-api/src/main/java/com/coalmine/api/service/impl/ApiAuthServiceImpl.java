package com.coalmine.api.service.impl;

import com.coalmine.api.domain.ApiAuth;
import com.coalmine.api.mapper.ApiAuthMapper;
import com.coalmine.api.service.IApiAuthService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * api权限表 服务实现类
 * </p>
 *
 * @author 尚郑
 * @since 2022-04-01
 */
@Service
public class ApiAuthServiceImpl extends ServiceImpl<ApiAuthMapper, ApiAuth> implements IApiAuthService {

}
