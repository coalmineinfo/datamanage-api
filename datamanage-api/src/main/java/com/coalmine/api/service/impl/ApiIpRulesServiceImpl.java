package com.coalmine.api.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.coalmine.api.domain.ApiIpRules;
import com.coalmine.api.mapper.ApiIpRulesMapper;
import com.coalmine.api.service.IApiIpRulesService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.coalmine.api.util.IPBackWhiteUtil;
import com.coalmine.api.util.IPRuleCache;
import com.coalmine.api.util.UUIDUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.annotation.PostConstruct;
import java.util.*;

/**
 * <p>
 * 系统黑白名单信息表 服务实现类
 * </p>
 *
 * @author 尚郑
 * @since 2022-04-01
 */
@Service
@Transactional
public class ApiIpRulesServiceImpl extends ServiceImpl<ApiIpRulesMapper, ApiIpRules> implements IApiIpRulesService {

    @Autowired
    private ApiIpRulesMapper ipMapper;

    /**
     * @param mode
     * @param ips   字符串
     */
    @Override
    public int on(String mode, String ips,String username) {
        int i = _on(mode, ips, username);
        // 设置缓存
        init();
        return i;
    }

    @Transactional
    public int _on(String mode, String ip,String username) {
        int i ;
        ipMapper.turnOn(mode, username);
        // 保存
        QueryWrapper<ApiIpRules> ipRulesQueryWrapper = new QueryWrapper<>();
        ipRulesQueryWrapper.eq("type", mode);
        ApiIpRules ipRules = ipMapper.selectOne(ipRulesQueryWrapper);
        if (ipRules == null) {
            String id = UUIDUtil.getUUID();
            i = ipMapper.saveIP(id, ip, mode, username);
        }else {
            ipRules.setType(mode);
            ipRules.setIp(ip);
            ipRules.setUpdateBy(username);
            ipRules.setUpdateTime(new Date());
            i = ipMapper.updateById(ipRules);
        }
        return i;
    }

    @Override
    public int off(String username) {
        int i = _off(username);
        // 设置缓存
        init();
        return i;
    }



    @Transactional
    public int _off(String username) {
        return ipMapper.turnOff(username);
    }


    @PostConstruct
    public void init() {
        String blackIP = ipMapper.getBlackIP();
        String whiteIP = ipMapper.getWhiteIP();
        if (StrUtil.isNotEmpty(blackIP)) {
            IPRuleCache.blackIPs = blackIP;
        } else {
            IPRuleCache.blackIPs = "";
        }
        if (StrUtil.isNotEmpty(whiteIP)) {
            IPRuleCache.whiteIPs = whiteIP;
        } else {
            IPRuleCache.whiteIPs = "";
        }
        Map<String, String> map = ipMapper.getStatus();
        IPRuleCache.mode = map.get("mode");
        IPRuleCache.status = map.get("status");
    }

    @Override
    public Map<String, String> detail() {
        List<Map<String, String>> ipRule = ipMapper.getIPRule();
        Map<String, String> status = ipMapper.getStatus();
        ipRule.stream().forEach(t -> {
            String type = t.get("type");
            String ip = t.get("ip");
            if (type.equals("white")) {
                status.put("whiteIP", ip);
            } else if (type.equals("black")) {
                status.put("blackIP", ip);
            }
        });

        return status;
    }

    /**
     * 校验ip
     * @param originIp
     * @return
     */
    public static boolean checkIP(String originIp) {
        if (IPRuleCache.status.equals("on")) {
            if (IPRuleCache.mode.equals("black")) {
                if (IPBackWhiteUtil.checkLoginIP(originIp,IPRuleCache.blackIPs)) {
                    return false;
                } else {
                    return true;
                }
            } else if (IPRuleCache.mode.equals("white")) {
                if (IPBackWhiteUtil.checkLoginIP(originIp,IPRuleCache.whiteIPs)) {
                    return true;
                } else {
                    return false;
                }
            }

        }
        return true;
    }

}
