package com.coalmine.api.service;

import com.coalmine.api.domain.ApiIpRules;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Map;

/**
 * <p>
 * 系统黑白名单信息表 服务类
 * </p>
 *
 * @author 尚郑
 * @since 2022-04-01
 */
public interface IApiIpRulesService extends IService<ApiIpRules> {

    Map<String, String> detail();

    int on(String mode, String ip, String username);

    int off(String username);

}
