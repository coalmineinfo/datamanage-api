package com.coalmine.api.service;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.coalmine.api.domain.ApiGroup;
import com.baomidou.mybatisplus.extension.service.IService;
import com.coalmine.common.core.domain.AjaxResult;

import java.util.List;
import java.util.Map;

/**
 * <p>
 * api组信息表 服务类
 * </p>
 *
 * @author 尚郑
 * @since 2022-04-01
 */
public interface IApiGroupService extends IService<ApiGroup> {


    AjaxResult addApiGroup(ApiGroup apiGroup);

    AjaxResult updateApiGroup(ApiGroup apiGroup);

    AjaxResult deleteApiGroupById(String id);

    ApiGroup findGroupById(String id);

    List<ApiGroup> getAll();


    List<ApiGroup> getAllGroupList();

    List<ApiGroup> selectBatch(List<String> ids);

    void insertBatch(List<ApiGroup> configs);

    List<Map> getAllGroup();

}
