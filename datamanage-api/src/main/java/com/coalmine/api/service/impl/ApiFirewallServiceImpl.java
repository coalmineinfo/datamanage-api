package com.coalmine.api.service.impl;

import com.coalmine.api.domain.ApiFirewall;
import com.coalmine.api.mapper.ApiFirewallMapper;
import com.coalmine.api.service.IApiFirewallService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 尚郑
 * @since 2022-04-01
 */
@Service
public class ApiFirewallServiceImpl extends ServiceImpl<ApiFirewallMapper, ApiFirewall> implements IApiFirewallService {

}
