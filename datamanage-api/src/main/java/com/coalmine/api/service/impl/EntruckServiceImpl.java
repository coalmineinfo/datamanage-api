package com.coalmine.api.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;

import com.coalmine.api.domain.Entruck;
import com.coalmine.api.service.EntruckService;
import com.coalmine.api.mapper.EntruckMapper;
import com.coalmine.api.util.GetConnectionUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

/**
* @author lhj
* @description 针对表【entruck(装车站管理)】的数据库操作Service实现
* @createDate 2022-10-11 18:12:39
*/
@Service
public class EntruckServiceImpl extends ServiceImpl<EntruckMapper, Entruck>
implements EntruckService{
    @Value("${yxxt.datasource.url}")
    private String url;
    @Value("${yxxt.datasource.username}")
    private String username;
    @Value("${yxxt.datasource.password}")
    private String password;

    @Override
    public boolean addEntruck(List<Entruck> addEntruckList) {
        Connection conn = null;
        try {
            conn = GetConnectionUtil.GetConnection(url, username, password);
            String sql ="INSERT INTO entruck (LoadStationNo,VehicleLaneNo,CoalType,SecondCoal,CurrentCoal,Status)\n" +
                    "VALUES (?,?,?,?,?,?)";
            PreparedStatement ps = conn.prepareStatement(sql);
            for (Entruck addEbtruck: addEntruckList) {
                ps.setString(1,addEbtruck.getLoadStationNo());
                ps.setString(2,addEbtruck.getVehicleLaneNo());
                ps.setString(3,addEbtruck.getCoalType());
                ps.setString(4,addEbtruck.getCoalType());
                ps.setString(5,addEbtruck.getCurrentCoal());
                ps.setString(6,addEbtruck.getStatus());
                ps.addBatch();
                ps.executeBatch();
            }
            return true;
        } catch (SQLException throwables) {
            if (conn!=null){
                GetConnectionUtil.releaseConnection(conn);
            }

            throwables.printStackTrace();
            return false;
        }


    }
}
