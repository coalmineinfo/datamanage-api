package com.coalmine.api.service;


import com.baomidou.mybatisplus.extension.service.IService;
import com.coalmine.api.domain.AddTransport;

import java.sql.SQLException;
import java.util.List;

/**
* @author lhj
* @description 针对表【transport(拉运信息)】的数据库操作Service
* @createDate 2022-10-10 15:26:11
*/
public interface TransportService extends IService<AddTransport> {


    boolean addTransport(List<AddTransport> transportList);
}
