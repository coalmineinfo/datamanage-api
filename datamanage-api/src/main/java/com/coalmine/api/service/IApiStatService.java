package com.coalmine.api.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.coalmine.api.domain.ApiStat;

import java.util.List;

public interface IApiStatService extends IService<ApiStat> {

    List<ApiStat> statByDay(String day);

    List<ApiStat> statByUser();

    ApiStat statByApiUser(String apiId);

    List<ApiStat> statByDayRange(String startDay,
                                 String endDay);

    List<ApiStat> statByMonth();
}
