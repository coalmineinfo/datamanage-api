package com.coalmine.api.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.coalmine.api.domain.ApiApprove;
import com.coalmine.api.enums.EApproveStatus;
import com.coalmine.api.mapper.ApiApplyMapper;
import com.coalmine.api.service.IApiApplyService;
import com.coalmine.api.util.UUIDUtil;
import com.coalmine.common.utils.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Date;
import java.util.List;

@Service
public class ApiApplyServiceImpl extends ServiceImpl<ApiApplyMapper,ApiApprove> implements IApiApplyService {

    @Autowired
    ApiApplyMapper apiApplyMapper;

    /**
     * 新增接口申请
     */
    @Transactional
    public int add(ApiApprove apiApprove ) {
        //申请人姓名
        apiApprove.setApplyUserName(SecurityUtils.getLoginUser().getUsername());
        //申请人id
        apiApprove.setApplyUserId(SecurityUtils.getLoginUser().getUserId());
        apiApprove.setId(UUIDUtil.getUUID());
        apiApprove.setApplyTime(new Date());
        apiApprove.setApproveType(EApproveStatus.PENDING.getCode());
        apiApprove.setApiId(apiApprove.getApiId());
        int row = apiApplyMapper.insert(apiApprove);
        return row;
    }

    @Override
    public List<ApiApprove> listByToken(String tokenId, String type) {
        return apiApplyMapper.listByToken(tokenId, type);
    }

    @Override
    public List<ApiApprove> listByTokenApi(String tokenId, String apiId) {
        return apiApplyMapper.listByTokenApi(tokenId, apiId);
    }


}
