package com.coalmine.api.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.coalmine.api.common.ApiLogDto;
import com.coalmine.api.domain.ApiLog;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface IApiLogService extends IService<ApiLog> {

    List<ApiLogDto> selectApiLogList(ApiLogDto apiLogDto);

    ApiLogDto getApiLog(String id);

    /**
     * 批量插入数据列表
     *
     * @param dataList 数据列表
     */
    void insertApiLogList(@Param("list") List<ApiLog> dataList);
}
