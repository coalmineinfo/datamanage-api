package com.coalmine.api.service.impl;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.coalmine.api.domain.Carriage;
import com.coalmine.api.domain.CarriageInfo;
import com.coalmine.api.service.CarriageService;
import com.coalmine.api.mapper.CarriageMapper;
import com.coalmine.api.util.GetConnectionUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

/**
 * @author lhj
 * @description 针对表【carriage(火车装车订单表)】的数据库操作Service实现
 * @createDate 2022-10-12 10:36:10
 */
@Service
public class CarriageServiceImpl extends ServiceImpl<CarriageMapper, Carriage>
        implements CarriageService {

    @Value("${yxxt.datasource.url}")
    private String url;
    @Value("${yxxt.datasource.username}")
    private String username;
    @Value("${yxxt.datasource.password}")
    private String password;



    @Override
    public boolean addCarriage(Carriage carriage) {
        Connection conn = null;
        try {
            conn = GetConnectionUtil.GetConnection(url, username, password);
         String sql = "INSERT INTO carriage (taskNum,loadLine,coalVariety,trainNum,arrivalStationTime,loadDate,loadTime,finishedTime,trainNumTotal,actualLoadTotal,deviationTotal,arriveStation,antifreezeSprayAmount,dustSprayAmount,bindingStartTime,bindingEndTime,bindingCarriageNum,bindingJump,taskState,weightTotal,partialLoad,grossWeightTotal)\n" +
                 "VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
         String sql1="INSERT INTO carriageinfo (taskNum,orderNum,carriageType,trainNum,`load`,actualLoad,grossWeight,deviation,partialLoad,loadTime,finishedTime,jumpTrainType,antifreezeSprayAmount,dustSprayAmount)VALUES (?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
         PreparedStatement ps = conn.prepareStatement(sql);

            //for (int i = 0; i < jsonObject.size(); i++) {
            ps.setObject(1,carriage.getTaskNum());
            ps.setObject(2,carriage.getLoadLine());
            ps.setObject(3,carriage.getCoalVariety());
            ps.setObject(4,carriage.getTrainNum());
            ps.setObject(5,carriage.getArrivalStationTime());
            ps.setObject(6,carriage.getLoadDate());
            ps.setObject(7,carriage.getLoadTime());
            ps.setObject(8,carriage.getFinishedTime());
            ps.setObject(9,carriage.getTrainNumTotal());
            ps.setObject(10,carriage.getActualLoadTotal());
            ps.setObject(11,carriage.getDeviationTotal());
            ps.setObject(12,carriage.getArriveStation());
            ps.setObject(13,carriage.getAntifreezeSprayAmount());
            ps.setObject(14,carriage.getDustSprayAmount());
            ps.setObject(15,carriage.getBindingStartTime());
            ps.setObject(16,carriage.getBindingEndTime());
            ps.setObject(17,carriage.getBindingCarriageNum());
            ps.setObject(18,carriage.getBindingJump());
            ps.setObject(19,carriage.getTaskState());
            ps.setObject(20,carriage.getWeightTotal());
            ps.setObject(21,carriage.getPartialLoad());
            ps.setObject(22,carriage.getGrossWeightTotal());
            ps.addBatch();
            ps.executeBatch();


            PreparedStatement ps1 = conn.prepareStatement(sql1);
            System.out.println(carriage.getCarriageInfo());
            for (CarriageInfo carriageInfo: carriage.getCarriageInfo()) {
                    ps1.setString(1, carriageInfo.getTaskNum());
                    ps1.setInt(2, carriageInfo.getOrderNum());
                    ps1.setString(3, carriageInfo.getCarriageType());
                    ps1.setInt(4, carriageInfo.getTrainNum());
                    ps1.setDouble(5, carriageInfo.getLoad());
                    ps1.setDouble(6, carriageInfo.getActualLoad());
                    ps1.setDouble(7, carriageInfo.getGrossWeight());
                    ps1.setDouble(8, carriageInfo.getDeviation());
                    ps1.setDouble(9, carriageInfo.getPartialLoad());
                    ps1.setString(10, carriageInfo.getLoadTime());
                    ps1.setString(11, carriageInfo.getFinishedTime());
                    ps1.setString(12, carriageInfo.getJumpTrainType());
                    ps1.setDouble(13, carriageInfo.getAntifreezeSprayAmount());
                    ps1.setDouble(14, carriageInfo.getDustSprayAmount());
                    ps1.addBatch();
                    ps1.executeBatch();
                    String taskNum = carriageInfo.getTaskNum();
                    System.out.println("==============" + taskNum);
                }
            //}
            return true;
        } catch (Exception throwables) {
            if (conn != null) {
                GetConnectionUtil.releaseConnection(conn);
            }
           throwables.printStackTrace();
            return false;
        }

    }
}




