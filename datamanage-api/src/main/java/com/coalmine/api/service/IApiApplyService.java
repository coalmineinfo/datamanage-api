package com.coalmine.api.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.coalmine.api.domain.ApiApprove;
import org.apache.ibatis.annotations.Param;

import java.util.List;

public interface IApiApplyService extends IService<ApiApprove> {

    /**
     * 新增接口申请
     */
    int add(ApiApprove apiApprove);

    List<ApiApprove> listByToken(@Param("tokenId") String tokenId, @Param("type") String type);

    List<ApiApprove> listByTokenApi(@Param("tokenId") String tokenId, @Param("apiId") String apiId);
}
