package com.coalmine.api.service;

import com.coalmine.api.domain.ApiToken;
import com.baomidou.mybatisplus.extension.service.IService;
import com.coalmine.common.core.domain.AjaxResult;

import java.util.List;

/**
 * <p>
 * api的token信息表 服务类
 * </p>
 *
 * @author 尚郑
 * @since 2022-04-01
 */
public interface IApiTokenService extends IService<ApiToken> {

    int insert(ApiToken token);

    List<String> getAuthGroups(String tokenId);

    List<ApiToken> getAll();

    ApiToken getToken(String tokenStr);

    int deleteById(String id);

    boolean auth(String tokenId, List<String> groupIds,String createBy);

    String getTokenByUser(String name, String password);

    ApiToken getToken(String name, String password);

    boolean checkToken(String api, String tokenId);

    //通过token_id获取token,判断token是否过期
    boolean checkTokenExpire(String tokenId);

    ApiToken getTokenById(String tokenId);

    int changePassword(String name, String oldPassword, String newPassword);
}
