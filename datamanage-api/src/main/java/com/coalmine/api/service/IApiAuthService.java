package com.coalmine.api.service;

import com.coalmine.api.domain.ApiAuth;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * api权限表 服务类
 * </p>
 *
 * @author 尚郑
 * @since 2022-04-01
 */
public interface IApiAuthService extends IService<ApiAuth> {

}
