package com.coalmine.api.util;

import org.apache.commons.lang3.RandomStringUtils;
import java.util.UUID;
public class UUIDUtil {

    public static String id() {
        return RandomStringUtils.random(8,true,true);
    }

    public static String getUUID(){
        return UUID.randomUUID().toString().replace("-", "");
    }

}
