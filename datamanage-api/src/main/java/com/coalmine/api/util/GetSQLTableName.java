package com.coalmine.api.util;





import cn.hutool.core.map.MapUtil;
import com.alibaba.druid.sql.SQLUtils;
import com.alibaba.druid.sql.ast.SQLStatement;
import com.alibaba.druid.sql.ast.statement.SQLExprTableSource;
import com.alibaba.druid.sql.ast.statement.SQLSelectQueryBlock;
import com.alibaba.druid.sql.ast.statement.SQLSelectStatement;
import com.alibaba.druid.sql.ast.statement.SQLTableSource;
import com.alibaba.druid.sql.dialect.mysql.parser.MySqlStatementParser;
import com.alibaba.druid.sql.dialect.mysql.visitor.MySqlSchemaStatVisitor;
import com.alibaba.druid.sql.parser.SQLStatementParser;
import com.alibaba.druid.sql.visitor.SchemaStatVisitor;
import com.alibaba.druid.stat.TableStat;
import com.baomidou.mybatisplus.annotation.DbType;
import com.coalmine.api.common.QueryModelInfo;
import com.github.freakchick.orange.SqlMeta;
import lombok.extern.slf4j.Slf4j;

import java.util.*;

@Slf4j
public class GetSQLTableName {

    public static QueryModelInfo getAllTableNameBySQL(String sql) {
       QueryModelInfo queryModelInfo = new QueryModelInfo();
        SQLStatementParser parser = new MySqlStatementParser(sql);
        // 使用Parser解析生成AST，这里SQLStatement就是AST
        SQLStatement sqlStatement = parser.parseStatement();
        MySqlSchemaStatVisitor visitor = new MySqlSchemaStatVisitor();
        sqlStatement.accept(visitor);
        Map<TableStat.Name, TableStat> tables = visitor.getTables();
        List<String> allTableName = new ArrayList<>();
        for (TableStat.Name t : tables.keySet()) {
            allTableName.add(t.getName());
        }
       //查询列
       Collection<TableStat.Column> columns = visitor.getColumns();
       List<String> columnList = new ArrayList<>();
       columns.stream().forEach(row -> {
           if(row.isSelect()){
               //保存select字段
               columnList.add(row.getName());
           }
       });
       queryModelInfo.setColumnList(columnList);
       queryModelInfo.setTableName(allTableName);
        return queryModelInfo;
    }


    public static void main(String[] args) throws Exception {
        String data1 = "SELECT \n" +
                "<if test=\"fields == null and fields == ''\" >\n" +
                "*\n" +
                "</if>\n" +
                "<if test=\"fields != null and fields != ''\" >\n" +
                "dev_lname , sys_name,sub_sys_name, dev_type, dev_name, ${fields}\n" +
                "</if>\n" +
                "FROM  <if test=\"sysCode == null or sysCode == '' \" >ods.ods_pdcxt</if> <if test=\"sysCode != null and sysCode != '' \" >ods.ods_${sysCode}</if>\n" +
                "<where> \n" +
                "<if test = \"startTime != null and startTime !='' and endTime!= null and endTime!=''\" > \n" +
                "data_time &gt;= #{startTime}\n" +
                "</if>\n" +
                "<if test = \"startTime != null and startTime !='' and endTime!= null and endTime!=''\" > \n" +
                "AND data_time &lt;= #{endTime}\n" +
                "</if>\n" +
                "<if test = \"sysName != null and sysName !=''\" > \n" +
                "AND sys_name = #{sysName}\n" +
                "</if>\n" +
                "<if test = \"subSysName != null and subSysName !=''\" > \n" +
                "AND sub_sys_name = #{subSysName}\n" +
                "</if>\n" +
                "<if test = \"devType != null and devType !=''\" > \n" +
                "AND dev_type = #{devType}\n" +
                "</if>\n" +
                "<if test = \"devName != null and devName !=''\" > \n" +
                "AND dev_name = #{devName}\n" +
                "</if>\n" +
                "<if test = \"devLname != null and devLname !=''\" > \n" +
                "AND dev_lname = #{devLname}\n" +
                "</if>\n" +
                "<if test = \"startTime == '' or endTime == '' or startTime == null or endTime == null\">\n" +
                "AND toYYYYMMDD(data_time) = toYYYYMMDD(now()) \n" +
                "AND  toHour(data_time) = toHour(now())\n" +
                "AND  toMinute(data_time) = toMinute(now()) \n" +
                "</if>\n" +
                "</where>\n" +
                "ORDER BY  create_time  DESC\n" +
                "\n";

        HashMap<String,Object> params = new HashMap<>();
        params.put("sysCode","zcgzmxt");
        params.put("subSysName","泵站系统");
        params.put("fields","voltage as v, charge as C,oil_pressure as o");
        params.put("startTime","2022-09-07 21:09:44");
        params.put("endTime","2022-09-07 21:09:54");

        SqlMeta parse = SqlEngineUtil.getEngine().parse(data1, params);
        System.out.println(parse.getSql());

        QueryModelInfo queryModelInfo = getAllTableNameBySQL(parse.getSql());
        queryModelInfo.getTableName().forEach(System.out::println);
    }
}
