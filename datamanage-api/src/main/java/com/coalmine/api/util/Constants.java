package com.coalmine.api.util;

public class Constants {
    public static String APP_JSON = "application/json";
    public static String APP_FORM_URLENCODED = "application/x-www-form-urlencoded";

    public static final String AUTH_TOKEN_GROUP_API_FLAG = "group";
}
