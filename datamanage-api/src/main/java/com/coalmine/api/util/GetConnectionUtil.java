package com.coalmine.api.util;

import lombok.extern.slf4j.Slf4j;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

@Slf4j
public class GetConnectionUtil  {


    public static Connection GetConnection (String url, String user, String password) throws SQLException {
        Connection conn = DriverManager.getConnection(url, user, password);
        return conn;
    }
    public static void releaseConnection(Connection conn) {
        try {
            if (conn != null)
                conn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}
