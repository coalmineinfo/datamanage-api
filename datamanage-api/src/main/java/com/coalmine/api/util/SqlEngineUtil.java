package com.coalmine.api.util;

import com.github.freakchick.orange.engine.DynamicSqlEngine;

/**
 * @program: dbApi
 * @description:
 * @author: pyq
 * @create: 2022-04-06 10:02
 **/
public class SqlEngineUtil {

    static DynamicSqlEngine engine = new DynamicSqlEngine();

    public static DynamicSqlEngine getEngine() {
        return engine;
    }
}
