package com.coalmine.api.config;

import com.coalmine.api.interceptor.LoginInterceptor;
import com.coalmine.common.core.redis.RedisCache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * 用于拦截未登录资源服务目录的
 */
@Configuration
public class LoginConfiguration implements WebMvcConfigurer {

    @Autowired
    RedisCache redisCache;

    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        registry.addInterceptor(new LoginInterceptor(redisCache));
    }
}
