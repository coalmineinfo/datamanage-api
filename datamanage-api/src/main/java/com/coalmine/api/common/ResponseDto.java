package com.coalmine.api.common;

import cn.hutool.http.HttpStatus;
import com.alibaba.fastjson.annotation.JSONField;
import com.alibaba.fastjson.serializer.SerializerFeature;
import lombok.Data;

import javax.servlet.http.HttpServletResponse;

/**
 * @program: api
 * @description:
 * @author: pyq
 * @create: 2022-4-6 11:22
 **/
@Data
public class ResponseDto {

    String msg;
    Integer code;
    @JSONField(serialzeFeatures = {SerializerFeature.WriteMapNullValue})
    Object data;



    public static ResponseDto apiSuccess(Object data) {
        ResponseDto dto = new ResponseDto();
        dto.setData(data);
        dto.setCode(HttpServletResponse.SC_OK);
        dto.setMsg("查询成功");
        return dto;

    }

    public static ResponseDto successWithMsg(String msg) {
        ResponseDto dto = new ResponseDto();
        dto.setData(null);
        dto.setCode(HttpServletResponse.SC_OK);
        dto.setMsg(msg);
        return dto;
    }

    public static ResponseDto successWithData(Object data) {
        ResponseDto dto = new ResponseDto();
        dto.setData(data);
        dto.setCode(HttpServletResponse.SC_OK);
        return dto;
    }

    public static ResponseDto fail(String msg) {
        ResponseDto dto = new ResponseDto();
        dto.setCode(HttpServletResponse.SC_INTERNAL_SERVER_ERROR);
        dto.setMsg(msg);
        return dto;
    }
    public static ResponseDto failWithCodeMsg(Integer code,String msg) {
        ResponseDto dto = new ResponseDto();
        dto.setCode(code);
        dto.setMsg(msg);
        return dto;
    }
    public String toJsonString() {
        final StringBuilder sb = new StringBuilder();
        sb.append("{")
                .append("\"msg\":\"").append(msg).append('\"')
                .append(", \"code\":").append(code)
                .append(", \"data\":").append(data)
                .append("}");
        return sb.toString();
    }


    public String toQueryString() {
        return data.toString();
    }


}