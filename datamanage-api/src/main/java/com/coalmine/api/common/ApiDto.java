package com.coalmine.api.common;

import lombok.Data;

@Data
public class ApiDto {
    String id;
    String name;
    String groupName;
}