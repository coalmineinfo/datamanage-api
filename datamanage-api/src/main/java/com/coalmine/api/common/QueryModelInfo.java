package com.coalmine.api.common;

import lombok.Data;

import java.util.List;
@Data
public class QueryModelInfo {

    private List<String> tableName;
    private List<String> columnList;



}
