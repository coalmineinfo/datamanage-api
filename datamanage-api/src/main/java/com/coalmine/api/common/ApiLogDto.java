package com.coalmine.api.common;

import com.coalmine.api.domain.ApiLog;
import com.fasterxml.jackson.annotation.JsonIgnore;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

@Data
@ApiModel(value = "ApiLogDto", description = "API日志信息")
public class ApiLogDto extends ApiLog {

    @ApiModelProperty(value = "接口路径", name = "path")
    private String path;

    @ApiModelProperty(value = "接口名称", name = "name")
    private String name;

    @ApiModelProperty(value = "token", name = "token")
    private String token;

    @ApiModelProperty(value = "token备注", name = "token")
    private String note;

    @ApiModelProperty(value = "开始时间", name = "startTime")
    @JsonIgnore
    private String startTime;

    @ApiModelProperty(value = "结束时间", name = "endTime")
    @JsonIgnore
    private String endTime;
}
