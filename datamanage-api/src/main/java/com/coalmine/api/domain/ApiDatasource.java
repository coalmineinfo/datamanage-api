package com.coalmine.api.domain;

import java.util.Date;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.coalmine.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NonNull;
import lombok.Value;

import javax.validation.constraints.NotEmpty;

/**
 * <p>
 *
 * </p>
 *
 * @author 尚郑
 * @since 2022-04-01
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "apidatasource", description = "数据源基本信息")
@TableName(value = "api_datasource")
public class ApiDatasource extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    // 主键生成(UUID,自增id,雪花算法,redis)
    // AUTO(数据库自增) ID_WORKER(默认雪花算法) ID_WORKER_STR(字符串表示)
    @TableId(type = IdType.UUID)
    @ApiModelProperty(value="主键id", name="id", hidden=true)
    private String id;

    /**
     * 名称
     */
    @TableField
    @NotEmpty
    @ApiModelProperty(value="数据源名称", name="name", required=true)
    private String name;

    /**
     * 描述
     */
    @TableField
    @ApiModelProperty(value="数据源描述", name="note", required=true)
    private String note;

    /**
     * 数据源类型
     */
    @TableField
    @NotEmpty
    @ApiModelProperty(value="数据源类型", name="type", required=true)
    private String type;

    /**
     * 连接url
     */
    @TableField
    @NotEmpty
    @ApiModelProperty(value="连接url", name="url", required=true)
    private String url;

    /**
     * 用户名
     */
    @TableField
    @NotEmpty
    @ApiModelProperty(value="用户名", name="username", required=true)
    private String username;

    /**
     * 密码
     */
    @TableField
    @NotEmpty
    @ApiModelProperty(value="密码", name="password", required=true)
    private String password;

    /**
     * true 修改密码 false不修改
     */
    @TableField(exist = false)
    @ApiModelProperty(value="修改密码", name="edit_password", required = true)
    boolean edit_password;

    /**
     * 驱动类
     */
    @TableField
    @NotEmpty
    @ApiModelProperty(value="驱动类", name="driver", required=true)
    private String driver;

    /**
     * 查询表的sql语句
     */
    @TableField(value = "table_sql")
    @NotEmpty
    @ApiModelProperty(value="查询表的sql语句", name="tableSql", required=true)
    private String tableSql;

    /**
     * 是否统计数据量    0 不统计  1 统计
     * 默认初始状态不统计
     */
    @TableField
    @ApiModelProperty(value="是否统计数据量", name="status", required=true)
    private Integer status;

}
