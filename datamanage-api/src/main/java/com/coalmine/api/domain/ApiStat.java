package com.coalmine.api.domain;

import com.alibaba.fastjson.JSONObject;
import lombok.Data;

/**
 * API统计数据
 */
@Data
public class ApiStat {

    private String token;

    private String apiId;

    private String day;

    private long successTimes;

    private long failureTimes;

    private String createTime;

    private String updateTime;

    public static ApiStat fromJson(String json) {
        return JSONObject.parseObject(json, ApiStat.class);
    }
}
