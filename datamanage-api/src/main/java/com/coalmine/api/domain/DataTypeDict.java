package com.coalmine.api.domain;

import lombok.Data;

@Data
public class DataTypeDict {

    private String typeName;

    private String subTypeName;

    private String tableName;

    private String tableComment;

    private String datasourceId;

    private String orders;
}
