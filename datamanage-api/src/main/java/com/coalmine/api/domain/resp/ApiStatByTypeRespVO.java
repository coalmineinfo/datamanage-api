package com.coalmine.api.domain.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * 根据时间类型统计响应数据
 *
 * @author wangleibo
 * @since 2022-09-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel("根据时间类型统计响应数据")
public class ApiStatByTypeRespVO implements Serializable {

    @ApiModelProperty(value = "日期", example = "2022-04-01")
    private String date;

    @ApiModelProperty(value = "成功调用次数", example = "10000")
    private long successTimes;

    @ApiModelProperty(value = "失败调用次数", example = "0")
    private long failureTimes;
}
