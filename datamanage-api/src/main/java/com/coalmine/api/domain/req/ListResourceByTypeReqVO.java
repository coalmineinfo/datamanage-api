package com.coalmine.api.domain.req;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * 用户申请资源列表请求数据
 *
 * @author wangleibo
 * @since 2022-09-05
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class ListResourceByTypeReqVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 申请状态：0-待处理 1-已通过 2-已拒绝
     */
    @ApiModelProperty(value = "申请状态：0-待处理 1-已通过 2-未通过（默认全部）", example = "0", required = false)
    private String type;

    /**
     * 分组ID
     */
    @ApiModelProperty(value = "分组ID", example = "xxxxx", required = false)
    private String groupId;

    /**
     * 资源名称
     */
    @ApiModelProperty(value = "资源名称", example = "xxxxx", required = false)
    private String resourceName;

    @ApiModelProperty(value = "起始页", required = true)
    private Integer pageNum;

    @ApiModelProperty(value = "页大小", required = true)
    private Integer pageSize;
}
