package com.coalmine.api.domain;

import java.util.Date;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.*;
import com.coalmine.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * api的sql信息表
 * </p>
 *
 * @author 尚郑
 * @since 2022-04-01
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "ApiSql", description = "api的sql信息表")
@TableName(value = "api_sql")
public class ApiSql extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @TableId(type = IdType.UUID)
    @ApiModelProperty(value="主键id", name="id", hidden=true)
    private String id;

    /**
     * api表id
     */
    @ApiModelProperty(value="api_config表id", name="apiId", hidden=true)
    @TableField("api_id")
    private String apiId;

    /**
     * sql文本
     */
    @ApiModelProperty(value="sql文本", name="sqlText",required =true)
    @TableField("sql_text")
    private String sqlText;


    /**
     * json返回结果
     */
    @ApiModelProperty(value="json返回结果", name="jsonResult", hidden=true)
    @TableField("json_result")
    private String jsonResult;

    /**
     * 数据转换插件类名
     */
    @ApiModelProperty(value="数据转换插件类名", name="transformPlugin")
    @TableField(value = "transform_plugin", insertStrategy = FieldStrategy.NOT_EMPTY)
    private String transformPlugin;

    /**
     * 数据转换插件参数
     */
    @ApiModelProperty(value="数据转换插件参数", name="transformPluginParams")
    @TableField(value = "transform_plugin_params")
    private String transformPluginParams;


}
