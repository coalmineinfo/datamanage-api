package com.coalmine.api.domain;

import java.io.Serializable;


import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * 装车站管理
 * @TableName entruck
 */
@Data
public class Entruck implements Serializable {
    /**
     * 装车站编号
     */
    @JsonProperty(value = "LoadStationNo")
    private String LoadStationNo;

    /**
     * 车道编号
     */
    @JsonProperty(value = "VehicleLaneNo")
    private String vehicleLaneNo;

    /**
     * 煤种类型
     */
    @JsonProperty(value = "CoalType")
    private String coalType;

    /**
     * 第二煤种
     */
    @JsonProperty(value = "SecondCoal")
    private String secondCoal;

    /**
     * 当前煤种
     */
    @JsonProperty(value = "CurrentCoal")
    private String currentCoal;

    /**
     * 状态
     */
    @JsonProperty(value = "Status")
    private String status;

}