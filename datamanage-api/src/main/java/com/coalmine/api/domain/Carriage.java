package com.coalmine.api.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * 火车装车订单表
 * @TableName carriage
 */
@TableName(value ="carriage")
@Data
public class Carriage implements Serializable {
    /**
     * 订单编号
     */
    @JsonProperty(value = "taskNum")
    private String taskNum;

    /**
     * 内环inside /外环outside
     */
    @JsonProperty(value = "loadLine")
    private String loadLine;

    /**
     * 煤种
     */
    @JsonProperty(value = "coalVariety")
    private String coalVariety;

    /**
     * 车次
     */
    @JsonProperty(value = "trainNum")
    private String trainNum;

    /**
     * 到站时间
     */
    @JsonProperty(value = "arrivalStationTime")
    private String arrivalStationTime;

    /**
     * 装车日期(yyyy-MM-dd)
     */
    @JsonProperty(value = "loadDate")
    private String loadDate;

    /**
     * 装车时间(yyyy-MM-dd HH:mm:ss)
     */
    @JsonProperty(value = "loadTime")
    private String loadTime;

    /**
     * 装完时间(yyyy-MM-dd HH:mm:ss)
     */
    @JsonProperty(value = "finishedTime")
    private String finishedTime;

    /**
     * 总车厢数
     */
    @JsonProperty(value = "trainNumTotal")
    private Integer trainNumTotal;

    /**
     * 实装吨数
     */
    @JsonProperty(value = "actualLoadTotal")
    private Double actualLoadTotal;

    /**
     * 总误差
     */
    @JsonProperty(value = "deviationTotal")
    private Double deviationTotal;

    /**
     * 到站
     */
    @JsonProperty(value = "arriveStation")
    private String arriveStation;

    /**
     * 防冻液喷洒量
     */
    @JsonProperty(value = "antifreezeSprayAmount")
    private Double antifreezeSprayAmount;

    /**
     * 抑尘剂喷洒量
     */
    @JsonProperty(value = "dustSprayAmount")
    private Double dustSprayAmount;

    /**
     * 加固开始时间( yyyy-MM-dd HH:mm:ss)
     */
    @JsonProperty(value = "bindingStartTime")
    private String bindingStartTime;

    /**
     * 加固结束时间( yyyy-MM-dd HH:mm:ss)
     */
    @JsonProperty(value = "bindingEndTime")
    private String bindingEndTime;

    /**
     * 加固车厢数(默认值：0)
     */
    @JsonProperty(value = "bindingCarriageNum")
    private Integer bindingCarriageNum;

    /**
     * 加固跳车数(默认值：0)
     */
    @JsonProperty(value = "bindingJump")
    private Integer bindingJump;

    /**
     * 4:装车完成
     */
    @JsonProperty(value = "taskState")
    private Integer taskState;

    /**
     * 
     */
    @JsonProperty(value = "weightTotal")
    private Double weightTotal;

    /**
     * 
     */
    @JsonProperty(value = "partialLoad")
    private Double partialLoad;

    /**
     * 
     */
    @JsonProperty(value = "grossWeightTotal")
    private Double grossWeightTotal;

    @JsonProperty(value = "carriageInfo")
    private List<CarriageInfo> carriageInfo;
}