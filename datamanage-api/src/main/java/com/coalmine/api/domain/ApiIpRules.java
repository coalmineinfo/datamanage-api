package com.coalmine.api.domain;

import java.util.Date;
import java.io.Serializable;

import com.coalmine.common.core.domain.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * 系统黑白名单信息表
 * </p>
 *
 * @author 尚郑
 * @since 2022-04-01
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class ApiIpRules extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    private String id;

    /**
     * 类型 白名单 white 黑名单 black
     */
    private String type;

    /**
     * ip地址
     */
    private String ip;





}
