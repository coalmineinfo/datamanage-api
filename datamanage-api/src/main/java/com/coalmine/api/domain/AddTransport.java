package com.coalmine.api.domain;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;

/**
 * 拉运信息
 * @TableName transport
 */
@Data
public class AddTransport implements Serializable {
    /**
     * 主键唯一标识
     */
    @JsonProperty(value = "PCCode")
    private Integer pccode;

    /**
     * 标的代码
     */
    @JsonProperty(value = "BCode")
    private String bcode;

    /**
     * 车号
     */
    @JsonProperty(value = "CarNo")
    private String carno;

    /**
     * 车型
     */
    @JsonProperty(value = "CarType")
    private String cartype;

    /**
     * 定重
     */
    @JsonProperty(value = "BasisWeight")
    private Double basisweight;

    /**
     * 司机姓名
     */
    @JsonProperty(value = "DriverName")
    private String drivername;

    /**
     * 司机电话
     */
    @JsonProperty(value = "Phone")
    private String phone;

    /**
     * 客户名称
     */
    @JsonProperty(value = "Customer")
    private String customer;

    /**
     * 卸货地址
     */
    @JsonProperty(value = "Adr")
    private String adr;

    /**
     * 煤种类型
     */
    @JsonProperty(value = "CoalType")
    private String coaltype;

    /**
     * 开票时间
     */
    @JsonProperty(value = "KTime")
    private String ktime;

    /**
     * 进磅时间
     */
    @JsonProperty(value = "ITime")
    private String itime;

    /**
     * 装车时间
     */
    @JsonProperty(value = "ZTime")
    private String ztime;

    /**
     * 出磅时间
     */
    @JsonProperty(value = "OTime")
    private String otime;

    /**
     * 皮重
     */
    @JsonProperty(value = "PWeight")
    private Double pweight;

    /**
     * 毛重
     */
    @JsonProperty(value = "MWeight")
    private Double mweight;

    /**
     * 净重
     */
    @JsonProperty(value = "JWeight")
    private Double jweight;
}