package com.coalmine.api.domain.resp;

import lombok.Data;

@Data
public class ApiStatRespVO {

    private String apiName;

    private long successTimes;
}
