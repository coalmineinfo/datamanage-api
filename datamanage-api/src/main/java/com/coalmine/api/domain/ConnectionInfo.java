package com.coalmine.api.domain;

import lombok.Data;

@Data
public class ConnectionInfo {

    private String url;

    private String host;

    private String port;

    private String dbName;
}
