package com.coalmine.api.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

/**
 * <p>
 *
 * </p>
 *
 * @author 尚郑
 * @since 2022-04-01
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "ApiConfig", description = "API基本信息")
@TableName(value = "api_config")
public class ApiConfig implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(type = IdType.UUID)
    @ApiModelProperty(value="主键id 更新必传", name="id")
    private String id;

    /**
     * 请求地址
     */
    @TableField
    @ApiModelProperty(value="接口路径", name="path", required=true)
    private String path;

    /**
     * 名称
     */
    @TableField
    @ApiModelProperty(value="接口名称", name="name", required=true)
    private String name;

    /**
     * 描述
     */
    @TableField
    @ApiModelProperty(value="接口描述", name="note")
    private String note;

    /**
     * 默认请求参数 application/x-www-form-urlencoded 类API对应的参数
     */
    @TableField(updateStrategy = FieldStrategy.IGNORED)
    @ApiModelProperty(value="接口请求参数", name="params",dataType ="JSON")
    private String params;

    /**
     * 状态 0 下线 1 上线
     */
    @TableField
    @ApiModelProperty(value="状态", name="status", hidden=true)
    private Integer status;

    /**
     * 数据源id
     */
    @TableField(value = "datasource_id")
    @ApiModelProperty(value="数据源id", name="datasourceId", required=true)
    private String datasourceId;

    /**
     * 权限 0 私有 1 公开
     */
    @TableField
    @ApiModelProperty(value="权限0-私有 1-公开", name="previlege", required=true)
    private Integer previlege;

    /**
     * api分组id
     */
    @TableField("group_id")
    @ApiModelProperty(value="api分组id", name="groupId", required=true)
    private String groupId;

    /**
     * 缓存插件
     */
    @TableField(value = "cache_plugin",insertStrategy = FieldStrategy.NOT_EMPTY)
    @ApiModelProperty(value="缓存插件", name="cachePlugin")
    private String cachePlugin;

    /**
     * 缓存插件参数
     */
    @TableField(value = "cache_plugin_params")
    @ApiModelProperty(value="缓存插件参数", name="cachePluginParams")
    private String cachePluginParams;

    /**
     * 请求体类型
     */
    @TableField(value = "content_type")
    @ApiModelProperty(value="请求参数类型", name="contentType",notes = "x-www-form-urlencoded或者application/json")
    private String contentType;

    /**
     * 事务 0 关闭 1 开启 	开启事务后，如果有多条SQL，多条SQL将在同一个事务内执行
     */
    @TableField("open_trans")
    @ApiModelProperty(value="事务0-关闭 1-开启 ", name="open_trans",notes = "默认关闭")
    private Integer openTrans;

    /**
     * json请求参数
     */
    @TableField(value = "json_param",updateStrategy = FieldStrategy.IGNORED)
    @ApiModelProperty(value="接口json请求参数", name="jsonParam",dataType = "JSON")
    private String jsonParam;


    /**
     * 创建者
     */
    @TableField(value = "create_by")
    @ApiModelProperty(value="创建者", name="createBy", hidden=true)
    private String createBy;

    /**
     * 创建时间
     */
    @TableField(value = "create_time")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value="创建时间", name="createTime", hidden=true)
    private Date createTime;

    /**
     * 更新者
     */
    @TableField(value = "update_by")
    @ApiModelProperty(value="更新者", name="update_by", hidden=true)
    private String updateBy;

    /**
     * 更新时间
     */
    @TableField(value = "update_time")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value="更新时间", name="update_time", hidden=true)
    private Date updateTime;

    @TableField(exist = false)
    @ApiModelProperty(value="api的sql信息表", name="sqlList", required = true)
    List<ApiSql> sqlList;

    @TableField(exist = false)
    @ApiModelProperty(value="邮箱", name="mail")
    String mail;

    @TableField(exist = false)
    String realServletPath;

}
