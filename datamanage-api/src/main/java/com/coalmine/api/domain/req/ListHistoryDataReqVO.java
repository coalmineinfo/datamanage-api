package com.coalmine.api.domain.req;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class ListHistoryDataReqVO {

    @NotNull(message = "数据源ID不能为空")
    private String datasourceId;

    @NotNull(message = "表名称不能为空")
    private String tableName;

    @NotNull(message = "分页索引不能为空")
    private int pageNum;

    @NotNull(message = "分页大小不能为空")
    private int pageSize;
}
