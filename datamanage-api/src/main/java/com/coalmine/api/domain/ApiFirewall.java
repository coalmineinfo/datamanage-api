package com.coalmine.api.domain;

import java.util.Date;
import java.io.Serializable;

import com.coalmine.common.core.domain.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 *
 * </p>
 *
 * @author 尚郑
 * @since 2022-04-01
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class ApiFirewall extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 状态 on 开启 off 关闭
     */
    private String status;

    /**
     * 类型 黑名单 black 白名单 white
     */
    private String mode;



}
