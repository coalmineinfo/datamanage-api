package com.coalmine.api.domain.req;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

@Data
public class TokenAuthReqVO {

    @NotNull(message = "token不能为空")
    private String tokenId;

    @NotNull(message = "分组不能为空")
    private List<String> groupIds;
}
