package com.coalmine.api.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 * pi调用日志信息表
 * </p>
 *
 * @author pyq
 * @since 2022-05-11
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "ApiLog", description = "api调用日志信息表")
@TableName(value = "api_log")
public class ApiLog implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @TableId(type = IdType.UUID)
    @ApiModelProperty(value="主键id", name="id", hidden=true)
    private String id;

    /**
     * api表id
     */
    @ApiModelProperty(value="api_config表id", name="apiId")
    @TableField("api_id")
    private String apiId;

    /**
     * token表主键ID
     */
    @ApiModelProperty(value="token表主键ID", name="tokenId")
    @TableField("token_id")
    private String tokenId;


    /**
     * 访问ip
     */
    @ApiModelProperty(value="访问ip", name="ip")
    @TableField("ip")
    private String ip;

    /**
     * 标识 true 成功 false 失败
     */
    @ApiModelProperty(value="标识 true 成功 false 失败", name="tag")
    @TableField(value = "tag")
    private Boolean tag;

    /**
     * 日志内容
     */
    @ApiModelProperty(value="日志内容", name="msg")
    @TableField(value = "msg")
    private String msg;


    /**
     * 创建时间
     */
    @ApiModelProperty(value="创建时间")
    @TableField(value = "create_time")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;




}
