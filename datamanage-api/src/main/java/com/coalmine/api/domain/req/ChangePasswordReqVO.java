package com.coalmine.api.domain.req;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 密码修改请求参数
 *
 * @author wangleibo
 * @since 2022-09-05
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class ChangePasswordReqVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户名
     */
    @NotNull(message = "用户名不能为空")
    private String username;

    /**
     * 旧密码
     */
    @NotNull(message = "旧不能为空")
    private String oldPassword;

    /**
     * 新密码
     */
    @NotNull(message = "新密码不能为空")
    private String newPassword;
}
