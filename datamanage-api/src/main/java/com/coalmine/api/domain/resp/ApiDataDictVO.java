package com.coalmine.api.domain.resp;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ApiDataDictVO {

    private String tableName;

    private String label;

    private String datasourceId;
}
