package com.coalmine.api.domain;


import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;

/**
 * 合同管理
 * @TableName contract
 */
@TableName(value ="contract")
@Data
public class Contract implements Serializable {
    /**
     * 标的代码
     */
    @JsonProperty(value = "BCode")
    private String BCode;

    /**
     * 客户名称
     */
    @JsonProperty(value = "Customer")
    private String customer;

    /**
     * 煤种类型
     */
    @JsonProperty(value = "CoalType")
    private String coalType;

    /**
     * 配额 带单位
     */
    @JsonProperty(value = "Quota")
    private String quota;

    /**
     * 签订时间
     */
    @JsonProperty(value = "SigTime")
    private String sigTime;

    /**
     * 合同开始时间
     */
    @JsonProperty(value = "STime")
    private String STime;

    /**
     * 合同结束时间
     */
    @JsonProperty(value = "ETime")
    private String ETime;

    /**
     * 价格  带单位
     */
    @JsonProperty(value = "Price")
    private Double price;

    /**
     * 区域类型
     */
    @JsonProperty(value = "AreaType")
    private String areaType;

    /**
     * 上站票（是、否）
     */
    @JsonProperty(value = "BoardingTicket")
    private String boardingTicket;

    /**
     * 余量
     */
    @JsonProperty(value = "Margin")
    private String margin;

    /**
     * 状态
     */
    @JsonProperty(value = "Status")
    private String status;


}