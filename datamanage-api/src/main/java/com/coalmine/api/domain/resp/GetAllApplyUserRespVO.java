package com.coalmine.api.domain.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * 获取审批用户响应数据
 *
 * @author wangleibo
 * @since 2022-09-12
 */
@Data
@AllArgsConstructor
@EqualsAndHashCode(callSuper = false)
@ApiModel("获取审批用户响应数据")
public class GetAllApplyUserRespVO implements Serializable {

    @ApiModelProperty(value = "TOKEN", example = "xxx")
    private String token;

    @ApiModelProperty(value = "用户名", example = "test")
    private String name;
}
