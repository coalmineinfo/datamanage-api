package com.coalmine.api.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.Date;

/**
 * 申请、审批表
 * @author 尚郑
 */
@Data
@EqualsAndHashCode(callSuper = false)
@TableName(value = "api_approve")
public class ApiApprove implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 主键
     */
    @TableId(type = IdType.UUID)
    private String id ;

    /**
     * api分组id
     */
    private String groupId;

    /**
     * tokenId (申请人)
     */
    private String tokenId;

    /**
     * 申请时间
     */
    @TableField(value = "apply_time")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value="申请时间", name="apply_time", hidden=true)
    private Date applyTime;

    /**
     * 申请原因
     */
    @NotNull(message = "申请原因不能为空")
    private String applyReason;

    /**
     * 审批人
     */
    private String approveName;

    /**
     * 审批时间
     */
    @TableField(value = "approve_time")
    @JsonFormat(locale = "zh", timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
    @ApiModelProperty(value="审批时间", name="approve_time", hidden=true)
    private Date approveTime;

    /**
     * 审批理由
     */
    private String approveReason;

    /**
     * 状态 0 未处理 1  已处理通过 2  已处理未通过
     */
    private Integer approveType;
    @TableField(exist = false)
    //虚拟字段
    private String apiName;
    @TableField(exist = false)
    //虚拟字段
    private String tokenName;

    //申请名称
    private String applyName;

    //申请人id，验证接口是否申请通过
    private Long applyUserId;

    //申请人姓名，验证接口是否申请通过
    private String applyUserName;

    //申请人
    private String apiId;

    //虚拟字段
    @TableField(exist = false)
    private String tokenStr;

}
