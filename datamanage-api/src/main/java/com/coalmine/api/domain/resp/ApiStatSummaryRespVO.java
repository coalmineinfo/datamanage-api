package com.coalmine.api.domain.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.List;

/**
 * 资源统计每日汇总响应数据
 *
 * @author wangleibo
 * @since 2022-09-12
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel("资源统计每日汇总响应数据")
public class ApiStatSummaryRespVO implements Serializable {

    @ApiModelProperty(value = "组名称", example = "A组")
    private String groupName;

    @ApiModelProperty(value = "成功调用次数", example = "10000")
    private long successTimes;

    // <apiName, successTimes>
    @ApiModelProperty(value = "API成功调用数据", example = "<api_A, 10000>")
    private List<ApiStatRespVO> apiStat;
}
