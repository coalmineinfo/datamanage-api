package com.coalmine.api.domain;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

/**
 * 加卸煤管理
 * @TableName addcoal
 */
@Data
public class Addcoal implements Serializable {
    /**
     * 主键id
     */
    @JsonProperty(value = "ID")
    private String ID;

    /**
     * 车号
     */
    @JsonProperty(value = "CarNo")
    private String carNo;

    /**
     * 煤种类型
     */
    @JsonProperty(value = "CoalType")
    private String coalType;

    /**
     * 加（卸）煤量（公斤）
     */
    @JsonProperty(value = "JXWeight")
    private Double JXWeight;

    /**
     * 完成时间
     */
    @JsonProperty(value = "CompleteTime")
    private String completeTime;

    /**
     * 加（卸）煤站
     */
    @JsonProperty(value = "CoalStation")
    private String coalStation;

    /**
     * 添加人
     */
    @JsonProperty(value = "RecordPeople")
    private String recordPeople;

    /**
     * 添加时间
     */
    @JsonProperty(value = "RecordTime")
    private String recordTime;


}