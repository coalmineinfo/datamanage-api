package com.coalmine.api.domain;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.coalmine.common.core.domain.BaseEntity;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;
import java.util.Date;

/**
 * <p>
 *
 * </p>
 *
 * @author zjx
 * @since 2022-04-08
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel(value = "apidatasourceconf", description = "数据源默认配置")
@TableName(value = "api_datasource_conf")
public class ApiDatasourceConf extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    // 主键生成(UUID,自增id,雪花算法,redis)
    // AUTO(数据库自增) ID_WORKER(默认雪花算法) ID_WORKER_STR(字符串表示)
    @TableId(type = IdType.UUID)
    @ApiModelProperty(value="主键id", name="id", hidden=true)
    private String id;

    /**
     * 数据源名称
     */
    @TableField
    @ApiModelProperty(value="数据源名称", name="name", required=true)
    private String name;

    /**
     * 连接的url
     */
    @TableField
    @ApiModelProperty(value="连接的url", name="url", required=true)
    private String url;

    /**
     * 驱动类
     */
    @TableField
    @ApiModelProperty(value="驱动类", name="driver", required=true)
    private String driver;

    /**
     * 查询库里所有表的sql语句
     */
    @TableField
    @ApiModelProperty(value="查询库里所有表的sql语句", name="sql", required=true)
    private String sql;




}
