package com.coalmine.api.domain.resp;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class ApiDataDictSubTypeVO {

    private String subType;

    private String label;

    private List<ApiDataDictVO> children;
}
