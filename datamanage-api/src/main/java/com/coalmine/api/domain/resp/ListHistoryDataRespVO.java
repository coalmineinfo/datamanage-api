package com.coalmine.api.domain.resp;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ListHistoryDataRespVO {

    private Object header;

    private Object rows;

    private long total;
}
