package com.coalmine.api.domain;

import java.util.Date;
import java.io.Serializable;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.coalmine.common.core.domain.BaseEntity;
import lombok.Data;
import lombok.EqualsAndHashCode;

/**
 * <p>
 * api的token信息表
 * </p>
 *
 * @author 尚郑
 * @since 2022-04-01
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class ApiToken extends BaseEntity {

    private static final long serialVersionUID = 1L;

    /**
     * 主键id
     */
    @TableId(type = IdType.UUID)
    private String id;

    /**
     * 获取token用户名
     */
    private String name;

    /**
     * 获取token密码
     */
    private String password;

    /**
     * token
     */
    private String token;

    /**
     * 描述
     */
    private String note;

    /**
     * 过期字段
     */
    private Long expire;



}
