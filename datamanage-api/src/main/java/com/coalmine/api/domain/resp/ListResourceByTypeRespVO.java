package com.coalmine.api.domain.resp;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.io.Serializable;

/**
 * 用户申请资源列表响应数据
 *
 * @author wangleibo
 * @since 2022-09-08
 */
@Data
@EqualsAndHashCode(callSuper = false)
@ApiModel("用户申请资源列表响应数据")
public class ListResourceByTypeRespVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 申请资源ID
     */
    @ApiModelProperty(value = "申请资源ID", example = "xxxxx")
    private String id;

    /**
     * 资源ID
     */
    @ApiModelProperty(value = "资源ID", example = "xxxxx")
    private String apiId;

    /**
     * 资源名称
     */
    @ApiModelProperty(value = "资源名称", example = "产量功效接口")
    private String apiName;

    /**
     * 资源组名称
     */
    @ApiModelProperty(value = "资源组名称", example = "XXX组")
    private String groupName;

    /**
     * 审批状态
     */
    @ApiModelProperty(value = "审批状态：0-待处理 1-已通过 2-未通过", example = "0")
    private String status;

    /**
     * 申请时间
     */
    @ApiModelProperty(value = "申请时间", example = "2022-01-01 12:00:00")
    private String applyTime;

    /**
     * 成功调用次数
     */
    @ApiModelProperty(value = "成功调用次数", example = "10000")
    private long successTimes;

    /**
     * 失败调用次数
     */
    @ApiModelProperty(value = "失败调用次数", example = "0")
    private long failureTimes;
}
