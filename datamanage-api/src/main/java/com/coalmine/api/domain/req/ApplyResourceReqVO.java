package com.coalmine.api.domain.req;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 资源申请请求参数
 *
 * @author wangleibo
 * @since 2022-08-30
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class ApplyResourceReqVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 资源ID
     */
    @ApiModelProperty(value = "资源ID", example = "xxxxx", required = true)
    @NotNull(message = "资源ID不能为空")
    private String apiId;

    /**
     * api分组id
     */
    @ApiModelProperty(value = "api分组id", example = "xxxxx", required = true)
    @NotNull(message = "api分组id不能为空")
    private String groupId;

    @ApiModelProperty(value = "申请原因", example = "xxxxx", required = true)
    @NotNull(message = "申请原因不能为空")
    private String applyReason;
}
