package com.coalmine.api.domain;

import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * 火车装车任务详情
 * @TableName carriageinfo
 */
@TableName(value ="carriageinfo")
@Data
public class CarriageInfo implements Serializable {
    /**
     * 订单编号（与任务主体的订单编号一致）
     */
    @JsonProperty(value = "taskNum")
    private String taskNum;

    /**
     * 序号
     */
    @JsonProperty(value = "orderNum")
    private Integer orderNum;

    /**
     * 车型
     */
    @JsonProperty(value = "carriageType")
    private String carriageType;

    /**
     * 车号
     */
    @JsonProperty(value = "trainNum")
    private Integer trainNum;

    /**
     * 目标载重
     */
    @JsonProperty(value = "load")
    private Double load;

    /**
     * 实际载重
     */
    @JsonProperty(value = "actualLoad")
    private Double actualLoad;

    /**
     * 实际毛重
     */
    @JsonProperty(value = "grossWeight")
    private Double grossWeight;

    /**
     * 误差
     */
    @JsonProperty(value = "deviation")
    private Double deviation;

    /**
     * 偏载量
     */
    @JsonProperty(value = "partialLoad")
    private Double partialLoad;

    /**
     * 装车时间
     */
    @JsonProperty(value = "loadTime")
    private String loadTime;

    /**
     * 装完时间
     */
    @JsonProperty(value = "finishedTime")
    private String finishedTime;

    /**
     * 是否跳车 true:跳车，false:不跳车
     */
    @JsonProperty(value = "jumpTrainType")
    private String jumpTrainType;

    /**
     * 防冻液喷洒量（L）
     */
    @JsonProperty(value = "antifreezeSprayAmount")
    private Double antifreezeSprayAmount;

    /**
     * 抑尘剂喷洒量（L）
     */
    @JsonProperty(value = "dustSprayAmount")
    private Double dustSprayAmount;

}