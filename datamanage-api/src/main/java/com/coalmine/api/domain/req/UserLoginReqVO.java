package com.coalmine.api.domain.req;

import lombok.Data;
import lombok.EqualsAndHashCode;

import javax.validation.constraints.NotNull;
import java.io.Serializable;

/**
 * 用户登录请求参数
 *
 * @author wangleibo
 * @since 2022-08-30
 */
@Data
@EqualsAndHashCode(callSuper = false)
public class UserLoginReqVO implements Serializable {

    private static final long serialVersionUID = 1L;

    /**
     * 用户名
     */
    @NotNull(message = "用户名不能为空")
    private String username;

    /**
     * 密码
     */
    @NotNull(message = "密码不能为空")
    private String password;
}
