package com.coalmine.api.domain.resp;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class ApiHistoryDataDictRespVO {

    private String type;

    private String label;

    private List<ApiDataDictSubTypeVO> children;
}