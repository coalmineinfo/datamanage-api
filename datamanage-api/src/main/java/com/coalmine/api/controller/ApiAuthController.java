package com.coalmine.api.controller;


import org.springframework.web.bind.annotation.RequestMapping;


import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * api权限表 前端控制器
 * </p>
 *
 * @author 尚郑
 * @since 2022-04-01
 */
@RestController
@RequestMapping("/api/auth")
public class ApiAuthController {

}
