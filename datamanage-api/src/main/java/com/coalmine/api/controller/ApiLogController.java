package com.coalmine.api.controller;


import com.coalmine.api.common.ApiLogDto;
import com.coalmine.api.domain.ApiToken;
import com.coalmine.api.service.IApiLogService;
import com.coalmine.api.service.IApiTokenService;
import com.coalmine.common.core.controller.BaseController;
import com.coalmine.common.core.domain.AjaxResult;
import com.coalmine.common.core.page.TableDataInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.*;
import java.util.stream.Collectors;

@Slf4j
@Api(tags = {"API-日志管理"})
@RestController
@RequestMapping("/api/log")
public class ApiLogController extends BaseController {

    @Autowired
    private IApiLogService iApiLogService;
    @Autowired
    IApiTokenService tokenService;

    /**
     * 获取api日志列表
     */
    @ApiOperation("获取日志列表")
    @PreAuthorize("@ss.hasPermi('api:log:list')")
    @GetMapping("/list")
    public TableDataInfo list(ApiLogDto apiLogDto) {
        startPage();
        List<ApiLogDto> list = iApiLogService.selectApiLogList(apiLogDto);
        return getDataTable(list);
    }

    @ApiOperation("获取token备注")
    @PreAuthorize("@ss.hasPermi('api:log:token')")
    @GetMapping("/token")
    public AjaxResult getTokenNote() {
        List<ApiToken> list = tokenService.list();
        List<ApiToken> fitterList = list.stream().filter(s -> s.getNote() != null)
                .collect(Collectors.toList());
        List<ApiToken> tokenList = fitterList.stream().collect(Collectors.collectingAndThen(
                Collectors.toCollection(() -> new TreeSet<>
                        (Comparator.comparing(ApiToken::getNote))), ArrayList::new));
        return AjaxResult.success(tokenList);
    }

    /**
     * 根据日志id获取详细信息
     */
    @ApiOperation("获取日志详情")
    @PreAuthorize("@ss.hasPermi('api:log:query')")
    @GetMapping(value = "detail/{id}")
    public AjaxResult getInfo(@PathVariable(value = "id", required = false) String id) {
        ApiLogDto apiLogDto = iApiLogService.getApiLog(id);
        if (Objects.equals(apiLogDto.getMsg(), "null")) {
            apiLogDto.setMsg(null);
        }
        if ((!apiLogDto.getTag()) && apiLogDto.getMsg() != null) {
            apiLogDto.setMsg(formatMsg(apiLogDto.getMsg()));
        }
        return AjaxResult.success(apiLogDto);
    }

    private String formatMsg(String msg) {
        final String splitToken = "######";
        msg = msg.replace("\"", "");
        msg = msg.replace("\\n", "######\n");
        msg = msg.replaceAll("\n" + splitToken, "");
        msg = msg.replaceAll("\n\\s+" + splitToken, "");
        msg = msg.replaceAll(splitToken, "");
        return msg;
    }
}
