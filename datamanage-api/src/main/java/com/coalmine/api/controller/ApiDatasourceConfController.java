package com.coalmine.api.controller;


import com.coalmine.api.domain.ApiDatasourceConf;
import com.coalmine.api.service.IApiDatasourceConfService;
import com.coalmine.common.core.controller.BaseController;
import com.coalmine.common.core.domain.AjaxResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;


/**
 * <p>
 *  api数据源信息 前端控制器
 * </p>
 *
 * @author 尚郑
 * @since 2022-04-01
 */
@Api(value="数据源配置",tags={"API数据源配置接口"})
@RestController
@RequestMapping("/api/dsconf")
public class ApiDatasourceConfController extends BaseController {

    @Autowired
    IApiDatasourceConfService apiDatasourceConfService;

    //@PreAuthorize("@ss.hasPermi('api:dsconf:list')")
    @ApiOperation("获取所有数据源配置")
    @GetMapping("/getAll")
    public AjaxResult getAll() {
        List<ApiDatasourceConf> list = apiDatasourceConfService.getAll();
        return AjaxResult.success(list);
    }

    //@PreAuthorize("@ss.hasPermi('api:dsconf:detail')")
    @ApiOperation("根据id获取数据源配置详情")
    @GetMapping("/detail/{id}")
    public AjaxResult detail(@PathVariable String id) {
        ApiDatasourceConf apiDatasourceConf = apiDatasourceConfService.detail(id);
        if(apiDatasourceConf == null){
                return AjaxResult.error("未查询id为"+id+"的数据");
        }
        return AjaxResult.success(apiDatasourceConf);
    }


}
