package com.coalmine.api.controller;



import com.coalmine.api.service.IApiIpRulesService;
import com.coalmine.common.annotation.Log;
import com.coalmine.common.constant.HttpStatus;
import com.coalmine.common.core.controller.BaseController;
import com.coalmine.common.core.domain.AjaxResult;
import com.coalmine.common.enums.BusinessType;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import java.util.Map;

/**
 * <p>
 *  系统防火墙 前端控制器
 * </p>
 *
 * @author 尚郑
 * @since 2022-04-01
 */

@RestController
@Api(tags = "API防火墙接口")
@RequestMapping("/api/firewall")
public class ApiFirewallController extends BaseController {

    @Autowired
    private IApiIpRulesService ipService;

    @PostMapping("/save")
    @ApiOperation("防火墙保存接口")
    @PreAuthorize("@ss.hasPermi('api:firewall:edit')")
    @Log(title = "防火墙修改", businessType = BusinessType.UPDATE)
    public AjaxResult save(@RequestBody Map<String,String> map) {
        String username = getUsername();
        if (map.get("status") == null || map.get("mode") == null || map.get("whiteIP") == null || map.get("blackIP") == null){
            return AjaxResult.error(HttpStatus.BAD_REQUEST, "请求参数有误");
        }
        String status = String.valueOf(map.get("status"));
        String mode = String.valueOf(map.get("mode"));
        String whiteIP = map.get("whiteIP");
        String blackIP = map.get("blackIP");

        if (status.equals("on")) {
            if (mode.equals("white")) {
                return toAjax(ipService.on(mode, whiteIP,username));
            } else if (mode.equals("black")) {
                return toAjax(ipService.on(mode, blackIP,username));
            }
        } else if (status.equals("off")) {
            return toAjax(ipService.off(username));
        }
        return AjaxResult.error(HttpStatus.BAD_REQUEST, "请求参数status有误");
    }

    @GetMapping("/detail")
    @ApiOperation("防火墙详情接口")
    @PreAuthorize("@ss.hasPermi('system:firewall:query')")
    public Map<String, String> detail() {
        return ipService.detail();
    }



}
