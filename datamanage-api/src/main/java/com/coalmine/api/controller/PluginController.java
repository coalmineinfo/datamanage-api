package com.coalmine.api.controller;

import com.alibaba.fastjson.JSONObject;
import com.coalmine.api.plugin.PluginManager;
import com.coalmine.common.core.domain.AjaxResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;

@Api(value="API插件接口",tags={"API插件接口"})
@RestController
@RequestMapping("api/plugin")
public class PluginController {

    @ApiOperation("获取api插件")
    @GetMapping("/all")
    public AjaxResult getAllCachePlugin(){
        Set<String> allCachePlugin = PluginManager.getAllCachePlugin();
        Set<String> allTransformPlugin = PluginManager.getAllTransResultSetPlugin();
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("cache",allCachePlugin);
        jsonObject.put("transform",allTransformPlugin);
        return AjaxResult.success(jsonObject);
    }

}
