package com.coalmine.api.controller;



import com.alibaba.fastjson.JSONObject;
import com.coalmine.api.domain.*;
import com.coalmine.api.service.*;
import com.coalmine.common.core.domain.AjaxResult;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;

import java.sql.SQLException;
import java.util.List;

@ApiOperation("插入json")
@RestController
@RequestMapping("/gateway/kmnf/yx/")
public class YxController {
    @Autowired
    TransportService transportService;
    @Autowired
    AddcoalService addcoalService;
    @Autowired
    EntruckService addEntruckService;
    @Autowired
    ContractService addContractService;
    @Autowired
    CarriageService addCarriageService;
//    @Autowired
//    CarriageinfoService addCarriageinfoService;

    @PostMapping("addTransport")
    @Transactional
    AjaxResult addTranspor(@RequestBody List<AddTransport> transportList) throws SQLException {
        boolean ret = transportService.addTransport(transportList);
        //System.out.println(transportList);
        return ret ? AjaxResult.success() : AjaxResult.error();
    }
    @PostMapping("addCoal")
    @Transactional
    public AjaxResult addCoal(@RequestBody List<Addcoal> addCoalList){
        boolean result= addcoalService.addCoal(addCoalList);
        return result ? AjaxResult.success() : AjaxResult.error("失败");

    }
    @PostMapping("addLoad")
    @Transactional
    public AjaxResult addEntruck(@RequestBody List<Entruck> addEntruckList){
        boolean result= addEntruckService.addEntruck(addEntruckList);
        return result ? AjaxResult.success() : AjaxResult.error("失败");

    }

    @PostMapping("addContract")
    @Transactional
    public  AjaxResult addContract(@RequestBody List<Contract> addContractList){
        boolean result=addContractService.addContract(addContractList);
        return result ? AjaxResult.success() : AjaxResult.error();

    }


    @PostMapping("SyncOrder")
    @Transactional
    public  AjaxResult addSyncOrder(@RequestBody Carriage carriage){
        boolean result=addCarriageService.addCarriage(carriage);
        return result ? AjaxResult.success() : AjaxResult.error();

    }





}
