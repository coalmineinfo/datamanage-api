package com.coalmine.api.plugin.dm;

import com.alibaba.fastjson.JSONObject;
import com.coalmine.api.plugin.TransResultSetPlugin;

import java.util.ArrayList;
import java.util.List;


public class ColumnValueTransPlugin extends TransResultSetPlugin {
    @Override
    public void init() {
        super.logger.info("ColumnValueTransPlugin init ...");
    }

    @Override
    public Object transResultSet(List<JSONObject> data, String params) {
        JSONObject jsonObject=new JSONObject();
        List<List<Object>> resultSet=new ArrayList<>();
        //String keys = StringUtils.join(data.get(0).keySet(), ",");
        Object[] columnArr= data.get(0).keySet().toArray();
        data.stream().forEach(t -> {
            resultSet.add(getJsonValues(t));
        });
        jsonObject.put("column",columnArr);
        jsonObject.put("values",resultSet);
        return jsonObject;

    }


    private static List<Object> getJsonValues(JSONObject jsonObject){
        List<Object> values =new ArrayList<>();
        jsonObject.forEach((k, value) -> {
            values.add(value);
        });
       // String result = values.stream().map(String::valueOf).collect(Collectors.joining(","));
        return values;
    }
}
