package com.coalmine.common.utils;


/**
 * @program: utilsdemo
 * @description: 判断运行的系统是windows还是linux
 * @author: pyq
 * @create: 2021-05-27 10:07
 */
public class JudgeSystem {


    /**
     * @description: 判断运行的系统是不是linux
     * @author: zhangyu
     * @create: 2019-08-12
     */
    public static boolean isLinux() {
        return System.getProperty("os.name").toLowerCase().contains("linux");
    }

    public static boolean isWindows() {
        return System.getProperty("os.name").toLowerCase().contains("windows");
    }

    public String JudgeSystem() {
        if (isLinux()) {
            return "linux";
        } else if (isWindows()) {
            return "windows";
        } else {
            return "other system";
        }
    }

}