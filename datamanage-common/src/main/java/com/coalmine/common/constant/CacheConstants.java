package com.coalmine.common.constant;

/**
 * 缓存KEY常量
 *
 * @author wangleibo
 * @since 2022-08-30
 */
public class CacheConstants {

    public static final String USER_TOKEN = "V1:USER:TOKEN";

    public static final String DATE_API_USER_STAT = "V1:DATE:API:USER:STAT";

    public static final String MONITOR_TABLE_ID = "monitor:table:id";

}