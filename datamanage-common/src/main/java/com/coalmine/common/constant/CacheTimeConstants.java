package com.coalmine.common.constant;

/**
 * 缓存时间常量
 *
 * @author wangleibo
 * @since 2022-08-30
 */
public class CacheTimeConstants {

    /**
     * 1分钟
     */
    public static final int TIME_1M = 1 * 60;

    /**
     * 2分钟
     */
    public static final int TIME_2M = 2 * 60;

    /**
     * 5分钟
     */
    public static final int TIME_5M = 5 * 60;
    /**
     * 10分钟
     */
    public static final int TIME_10M = 10 * 60;
    /**
     * 30分钟
     */
    public static final int TIME_30M = 30 * 60;
    /**
     * 1小时
     */
    public static final int TIME_1H = 60 * 60;
    /**
     * 2小时
     */
    public static final int TIME_2H = 2 * 60 * 60;
    /**
     * 8小时
     */
    public static final int TIME_8H = 8 * 60 * 60;
    /**
     * 12小时
     */
    public static final int TIME_12H = 12 * 60 * 60;
    /**
     * 24小时
     */
    public static final int TIME_24H = 24 * 60 * 60;
    /**
     * 7天
     */
    public static final int TIME_7D = 7 * 24 * 60 * 60;
    /**
     * 14天
     */
    public static final int TIME_14D = 14 * 24 * 60 * 60;
    /**
     * 30天
     */
    public static final int TIME_30D = 30 * 24 * 60 * 60;

    /**
     * 31天
     */
    public static final int TIME_31D = 31 * 24 * 60 * 60;

    /**
     * 60天
     */
    public static final int TIME_60D = 60 * 24 * 60 * 60;
}