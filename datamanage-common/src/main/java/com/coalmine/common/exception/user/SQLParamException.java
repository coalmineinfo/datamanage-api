package com.coalmine.common.exception.user;

import com.coalmine.common.exception.base.BaseException;

/**
 * 业务异常
 * 
 * @author pyq
 */
public class SQLParamException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    /**
     * 错误码
     */
    private Integer code;

    /**
     * 错误提示
     */
    private String message;

    /**
     * 错误明细，内部调试错误
     *
     * 和 {@link} 一致的设计
     */
    private String detailMessage;

    /**
     * 空构造方法，避免反序列化问题
     */
    public SQLParamException()
    {
    }

    public SQLParamException(String message)
    {
        this.message = message;
    }

    public SQLParamException(String message, Integer code)
    {
        this.message = message;
        this.code = code;
    }

    public String getDetailMessage()
    {
        return detailMessage;
    }

    public String getMessage()
    {
        return message;
    }

    public Integer getCode()
    {
        return code;
    }

    public SQLParamException setMessage(String message)
    {
        this.message = message;
        return this;
    }

    public SQLParamException setDetailMessage(String detailMessage)
    {
        this.detailMessage = detailMessage;
        return this;
    }

}