package com.coalmine.common.enums;

/**
 * 数据源
 * 
 * @author pyq
 */
public enum DataSourceType
{
    /**
     * 主库
     */
    MASTER,

    /**
     * 从库
     */
    SLAVE
}
